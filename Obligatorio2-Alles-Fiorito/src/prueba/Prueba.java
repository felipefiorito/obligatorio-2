
package prueba;

import dominio.Perro;
import dominio.Persona;
import dominio.Sistema;
import interfaz.VentanaLogin;

public class Prueba {
    
   public static void main(String[] args) {
        
       Sistema sistema = new Sistema();
       Persona persona = new Persona("Felipe", "felipe@mail.com");
       sistema.agregarPersona(persona);
       Perro perro = new Perro("Juan", 1, "Fila", 3, 20, "comentario","imagenes/perro.gif");
       sistema.agregarPerro(perro);
               
       VentanaLogin vent = new VentanaLogin(sistema);
       vent.setVisible(true);
        
    }
    
}
