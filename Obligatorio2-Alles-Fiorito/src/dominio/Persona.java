
package dominio;

public class Persona {
    
    private String nombre;
    private String mail;
    private String direccionImagen;
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    
    public String getDireccionImagen(){
        return direccionImagen;
    }
    
    public void setDireccionImagen(String path){
        direccionImagen = path;
    }
    
    public Persona(String nombre, String mail, String direccionImagen){
        this.setNombre(nombre);
        this.setMail(mail);
        this.setDireccionImagen(direccionImagen);
    }
    
    public Persona(String nombre, String mail){
        this.setNombre(nombre);
        this.setMail(mail);
        this.setDireccionImagen("imagenes/persona.png");
    }
    
    public Persona(String mail){
        this.setMail(mail);
    }
    
    @Override
    public String toString() {
        return this.getNombre() + " - " + this.getMail();
    }
    
    @Override
    public boolean equals(Object obj) {
        Persona persona = (Persona) obj;
        return (this.getMail().equals(persona.getMail()));
    }
}
