
package dominio;

public class Perro {
    
    private String nombre;
    private float edad;
    private String raza;
    private float altura;
    private float peso;
    private String comentarios;
    private String direccionImagen;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getEdad() {
        return edad;
    }

    public void setEdad(float edad) {
        this.edad = edad;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }
    
    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }
    
    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
    
    public String getDireccionImagen(){
        return direccionImagen;
    }
    
    public void setDireccionImagen(String path){
        direccionImagen = path;
    }
    
    public Perro(String nombre, float edad, String raza, float altura, 
            float peso, String comentario, String direccionImagen) {
        this.setNombre(nombre);
        this.setEdad(edad);
        this.setRaza(raza);
        this.setAltura(altura);
        this.setPeso(peso);
        this.setComentarios(comentario);
        this.setDireccionImagen(direccionImagen);
    }
    
    public Perro(String nombre) {
        this.setNombre(nombre);
    }
    
    @Override
    public String toString() {
        return this.getNombre();
    }

    @Override
    public boolean equals(Object obj) {
        Perro perro = (Perro) obj;
        return (this.getNombre().equals(perro.getNombre()));
    }
}
