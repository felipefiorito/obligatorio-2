
package dominio;

import java.util.Date;

public class Actividad {
    
    private String tipoActividad;
    private Perro perro;
    private Persona persona;
    private Date fecha;
    private String horario;
    private String alimento;
    private String servicio;
    private String mapaPaseo;
    
    public void setTipoActividad(String tipoActividad) {
        this.tipoActividad = tipoActividad;
    }
    
    public String getTipoActividad() {
        return tipoActividad;
    }
    
    public void setPerro(Perro perro){
        this.perro = perro;
    }
    
    public Perro getPerro(){
        return perro;
    }
    
    public void setPersona(Persona persona){
        this.persona = persona;
    }
    
    public Persona getPersona(){
        return persona;
    }
    
    public void setFecha(Date fecha){
        this.fecha = fecha;
    }
    
    public Date getFecha(){
        return fecha;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
    
    public String getHorario() {
        return horario;
    }
    
    public void setAlimento(String alimento) {
        this.alimento = alimento;
    }
    
    public String getAlimento() {
        return alimento;
    }
    
    public void setServicio(String servicio) {
        this.servicio = servicio;
    }
    
    public String getServicio() {
        return servicio;
    }
    
    public void setMapaPaseo(String mapaPaseo) {
        this.mapaPaseo = mapaPaseo;
    }
    
    public String getMapaPaseo() {
        return mapaPaseo;
    }
    
    public Actividad(){
    }
    
    public Actividad(Perro perro, Persona persona,Date fecha, String hora,
            String tipoActividad){
        this.setPerro(perro);
        this.setPersona(persona);
        this.setFecha(fecha);
        this.setHorario(hora);
        this.setTipoActividad(tipoActividad);    
    }

    @Override
    public boolean equals(Object obj) {
        Actividad actividad = (Actividad) obj;
        return (this.getTipoActividad().equals(actividad.getTipoActividad()))
                &&(this.getPerro().equals(actividad.getPerro()))
                    && (this.getPersona().equals(actividad.getPersona()))
                        &&(this.getFecha().equals(actividad.getFecha()));
    }
}
