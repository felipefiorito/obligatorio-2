package dominio;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sistema {

    private ArrayList<Perro> listaPerros;
    private ArrayList<Persona> listaPersonas;
    private ArrayList<Actividad> listaActividades;
    private ArrayList<Actividad> listaActividadesRealizadas;

    public Sistema() {
        listaPerros = new ArrayList<Perro>();
        listaPersonas = new ArrayList<Persona>();
        listaActividades = new ArrayList<Actividad>();
        listaActividadesRealizadas = new ArrayList<Actividad>();
    }

    public ArrayList<Perro> getListaPerros() {
        return listaPerros;
    }

    public void agregarPerro(Perro perro) {
        this.getListaPerros().add(perro);
    }

    public void agregarUnPerro(String nombre, float edad, String raza,
            float altura, float peso, String comentario, String direccionImagen) {
        Perro perro = new Perro(nombre, edad, raza, altura, peso,
                comentario, direccionImagen);
        this.getListaPerros().add(perro);
    }

    public void borrarPerro(Perro perro) {
        this.auxiliarBorrarPerro(perro);
        this.getListaPerros().remove(perro);
    }

    public boolean hayPerro() {
        return !(this.getListaPerros().isEmpty());
    }

    public boolean existeNombrePerro(String nombre) {
        Perro perro = new Perro(nombre);
        return this.getListaPerros().contains(perro);
    }

    public Perro getPerro(int pos) {
        return this.getListaPerros().get(pos);
    }

    public ArrayList<Persona> getListaPersonas() {
        return listaPersonas;
    }

    public void agregarPersona(Persona persona) {
        this.getListaPersonas().add(persona);
    }

    public void agregarUnaPersona(String nombre, String mail,
            String direccionImagen) {
        Persona persona = new Persona(nombre, mail, direccionImagen);
        this.getListaPersonas().add(persona);
    }

    public void borrarPersona(Persona persona) {
        this.auxiliarBorrarPersona(persona);
        this.getListaPersonas().remove(persona);
    }

    public boolean hayPersona() {
        return !(this.getListaPersonas().isEmpty());
    }

    public boolean existeMailPersona(String mail) {
        Persona persona = new Persona(mail);
        return this.getListaPersonas().contains(persona);
    }

    public Persona getPersona(int pos) {
        return this.getListaPersonas().get(pos);
    }

    public ArrayList<Actividad> getListaActividades() {
        return listaActividades;
    }

    public void agregarActividad(Actividad actividad) {
        this.getListaActividades().add(actividad);
    }

    public Actividad getActividad(int pos) {
        return this.getListaActividades().get(pos);
    }

    public ArrayList<Actividad> getListaActividadesRealizadas() {
        return listaActividadesRealizadas;
    }

    public void agregarActividadRealizada(Actividad actividad) {
        this.getListaActividadesRealizadas().add(actividad);
    }

    public Actividad getActividadRealizada(int pos) {
        return this.getListaActividadesRealizadas().get(pos);
    }

    public int cantidadNotificaciones(ArrayList<Actividad> actividades,
            Persona persona) {
        int cantidadNotificaciones = 0;
        for (int i = 0; i < actividades.size(); i++) {
            if (actividades.get(i).getPersona().equals(persona)) {
                cantidadNotificaciones++;
            }
        }
        return cantidadNotificaciones;
    }

    public String fechasNotificaciones(ArrayList<Actividad> actividades,
            Persona persona) {
        String fechasNotificaciones = "";
        for (int i = 0; i < actividades.size(); i++) {
            if (actividades.get(i).getPersona().equals(persona)) {
                fechasNotificaciones += actividades.get(i).getFecha().toString()
                    + "\n";
            }
        }
        return fechasNotificaciones;
    }

    public ArrayList<String> actividadesPorFecha(Date fecha,
            ArrayList<Actividad> actividades) {
        ArrayList<String> actividadesPorFecha = new ArrayList<>();
        for (int i = 0; i < actividades.size(); i++) {
            if (hayFecha(fecha)) {
                actividadesPorFecha.add(actividades.get(i).getPerro() + " - "
                        + actividades.get(i).getTipoActividad());
            }
        }
        return actividadesPorFecha;
    }

    public ArrayList<String> actividadesRealizadasPorFecha(Date fecha,
            ArrayList<Actividad> actividades) {
        ArrayList<String> actividadesPorFecha = new ArrayList<>();
        for (int i = 0; i < actividades.size(); i++) {
            if (hayFechaRealizadas(fecha)) {
                actividadesPorFecha.add(actividades.get(i).getPerro() + " - "
                        + actividades.get(i).getTipoActividad());
            }
        }
        return actividadesPorFecha;
    }

    public String detallesActRegistradaSeleccionada(int index,
            Date fecha, ArrayList<Actividad> actividades) {
        ArrayList<Actividad> actividadesPorFecha = new ArrayList<>();
        for (int i = 0; i < actividades.size(); i++) {
            if (hayFechaRealizadas(fecha)) {
                actividadesPorFecha.add(actividades.get(i));
            }
        }
        Actividad a = actividadesPorFecha.get(index);
        if (a.getAlimento() != null) {
            String alimento = a.getAlimento();
        }
        String fechaString = fecha.toString().substring(0, 10);
        String detalles = "> Perro: " + a.getPerro() + '\n'
                + "> Tipo de actividad: " + a.getTipoActividad() + '\n'
                + "> Persona asociada: " + a.getPersona() + '\n'
                + "> Dia: " + fechaString + '\n';
        if (a.getAlimento() != null) {
            detalles = detalles + "> Alimento: " + a.getAlimento();
        }
        return detalles;
    }
    
    public String detallesActSeleccionada(int index,
            Date fecha, ArrayList<Actividad> actividades) {
        ArrayList<Actividad> actividadesPorFecha = new ArrayList<>();
        for (int i = 0; i < actividades.size(); i++) {
            if (hayFecha(fecha)) {
                actividadesPorFecha.add(actividades.get(i));
            }
        }
        Actividad a = actividadesPorFecha.get(index);
        if (a.getAlimento() != null) {
            String alimento = a.getAlimento();
        }
        String fechaString = fecha.toString().substring(0, 10);
        String detalles = "> Perro: " + a.getPerro() + '\n'
                + "> Tipo de actividad: " + a.getTipoActividad() + '\n'
                + "> Persona asociada: " + a.getPersona() + '\n'
                + "> Dia: " + fechaString + '\n';
        if (a.getAlimento() != null) {
            detalles = detalles + "> Alimento: " + a.getAlimento();
        }
        return detalles;
    }

    public void auxiliarBorrarPerro(Perro perro) {
        Iterator<Actividad> iterador = this.getListaActividades().iterator();
        while (iterador.hasNext()) {
            Actividad actividad = iterador.next();
            if (actividad.getPerro().equals(perro)) {
                iterador.remove();
            }
        }
    }

    public void auxiliarBorrarPersona(Persona persona) {
        Iterator<Actividad> iterador = this.getListaActividades().iterator();
        while (iterador.hasNext()) {
            Actividad actividad = iterador.next();
            if (actividad.getPersona().equals(persona)) {
                iterador.remove();
            }
        }
    }

    public boolean existeAlimentacion(Date fecha, String alimento,
            String hora, int indexPerro) {
        boolean existeAlimentacion = false;
        Perro perro = this.getListaPerros().get(indexPerro);
        for (int i = 0; i < this.getListaActividades().size(); i++) {
            Actividad a = this.getListaActividades().get(i);
            existeAlimentacion = hayFecha(fecha) && a.getHorario().equals(hora)
                    && a.getPerro().equals(perro);
        }
        return existeAlimentacion;
    }

    public void registrarActividad(int perro, int persona, Date fecha,
            String hora, String tipo, String alimento) {
        Perro perr = this.getListaPerros().get(perro);
        Persona pers = this.getListaPersonas().get(persona);
        Actividad paseo = new Actividad(perr, pers, fecha, hora, tipo);
        paseo.setAlimento(alimento);
        agregarActividad(paseo);
    }

    public void registrarActividadRealizada(int perro, int persona, Date fecha,
            String hora, String tipo) {
        Perro perr = this.getListaPerros().get(perro);
        Persona pers = this.getListaPersonas().get(persona);
        Actividad actividad = new Actividad(perr, pers, fecha, hora, tipo);
        agregarActividadRealizada(actividad);
    }

    public void registrarPaseoRealizado(int perro, int persona, Date fecha,
            String hora, String tipo, String mapaPaseo) {
        Perro perr = this.getListaPerros().get(perro);
        Persona pers = this.getListaPersonas().get(persona);
        Actividad actividad = new Actividad(perr, pers, fecha, hora, tipo);
        actividad.setMapaPaseo(mapaPaseo);
        agregarActividadRealizada(actividad);
    }

    public boolean existePaseo(ArrayList<Actividad> actividades,
            Date fecha, String hora) {
        boolean existePaseo = false;
        for (int i = 0; i < actividades.size(); i++) {
            existePaseo = hayFecha(fecha)
                    && actividades.get(i).getHorario().equals(hora);
        }
        return existePaseo;
    }

    public boolean hayFecha(Date fecha1) {
        boolean esta = false;
        for (int i = 0; i < this.getListaActividades().size(); i++) {
            Actividad a = this.getListaActividades().get(i);
            if (comparoFecha(a.getFecha(), fecha1)) {
                esta = true;
            }
        }
        return esta;
    }
    
    public boolean hayFechaRealizadas(Date fecha1){
        boolean esta = false;
        for (int i = 0; i < this.getListaActividadesRealizadas().size(); i++) {
            Actividad a = this.getListaActividadesRealizadas().get(i);
            if (comparoFecha(a.getFecha(), fecha1)) {
                esta = true;
            }
        }
        return esta;
    }

    public boolean comparoFecha(Date fecha1, Date fecha2) {

        String mesDia1 = fecha1.toString().substring(0, 10);
        String mesDia2 = fecha2.toString().substring(0, 10);
        if (mesDia1.equals(mesDia2)) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean edadPermitida(float edad){
        return !(edad < 0 || edad > 40);
    }
    
    public boolean alturaPermitida(float altura){
        return !(altura < 5 || altura > 200);
    }
    
    public boolean pesoPermitido(float peso){
        return !(peso < 0.3 || peso > 150);
    }
    
    public boolean mailPermitido(String mail){
        Pattern patron = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher retorno = patron.matcher(mail);
        return retorno.find();
    }
    
    public boolean horaPermitida(int hora){
        return !(hora>23 || hora <00);
    }
    
    public boolean minutosPermitida(int minutos){
        return !(minutos>59 || minutos <00);
    }
}
