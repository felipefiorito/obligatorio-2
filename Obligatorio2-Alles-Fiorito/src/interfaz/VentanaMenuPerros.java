
package interfaz;

import javax.swing.*;
import dominio.Sistema;

public class VentanaMenuPerros extends javax.swing.JFrame {

    private Sistema modelo;
   
    public VentanaMenuPerros(Sistema unSistema) {
        initComponents();
        setModelo(unSistema);
        cargarLista();        
    }

    public Sistema getModelo(){
        return modelo;
    }
    
    public void setModelo(Sistema unModelo){
        this.modelo =unModelo;
    }
    
    public void cargarLista(){
        jListPerros.removeAll();
        DefaultListModel modeloLista = new DefaultListModel();
        for(int i=0; i<modelo.getListaPerros().size();i++){
            modeloLista.addElement(modelo.getPerro(i));          
        }
        jListPerros.setModel(modeloLista);
        jBtnEditar.setEnabled(modelo.hayPerro());
        jListPerros.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelPerros = new javax.swing.JPanel();
        jBtnAgregar = new javax.swing.JButton();
        jBtnBorrar = new javax.swing.JButton();
        jBtnEditar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListPerros = new javax.swing.JList();
        jButtonVolver = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelPerros.setBackground(new java.awt.Color(143, 0, 0));

        jBtnAgregar.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jBtnAgregar.setText("Agregar");
        jBtnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAgregarActionPerformed(evt);
            }
        });

        jBtnBorrar.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jBtnBorrar.setText("Borrar");
        jBtnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnBorrarActionPerformed(evt);
            }
        });

        jBtnEditar.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jBtnEditar.setText("Editar");
        jBtnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnEditarActionPerformed(evt);
            }
        });

        jListPerros.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jScrollPane2.setViewportView(jListPerros);

        jButtonVolver.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonVolver.setText("Volver");
        jButtonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelPerrosLayout = new javax.swing.GroupLayout(jPanelPerros);
        jPanelPerros.setLayout(jPanelPerrosLayout);
        jPanelPerrosLayout.setHorizontalGroup(
            jPanelPerrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPerrosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelPerrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButtonVolver, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBtnAgregar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBtnBorrar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBtnEditar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 166, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45))
        );
        jPanelPerrosLayout.setVerticalGroup(
            jPanelPerrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPerrosLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(jPanelPerrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelPerrosLayout.createSequentialGroup()
                        .addComponent(jBtnAgregar)
                        .addGap(18, 18, 18)
                        .addComponent(jBtnBorrar)
                        .addGap(18, 18, 18)
                        .addComponent(jBtnEditar)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(jButtonVolver)
                .addGap(34, 34, 34))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPerros, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPerros, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAgregarActionPerformed
        VentanaAgregarPerro vent = new VentanaAgregarPerro(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jBtnAgregarActionPerformed

    private void jButtonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverActionPerformed
        VentanaPrincipal vent = new VentanaPrincipal(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonVolverActionPerformed

    private void jBtnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnBorrarActionPerformed
        if(jListPerros.getSelectedIndex()!=-1){
            modelo.borrarPerro(modelo.getPerro(jListPerros.getSelectedIndex()));
        }else{
            JOptionPane.showMessageDialog(rootPane, 
                    "Debe seleccionar un perro para poder borrarlo");
        }
        cargarLista();
    }//GEN-LAST:event_jBtnBorrarActionPerformed

    private void jBtnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnEditarActionPerformed
        if(jListPerros.getSelectedIndex()!=-1){
            int indice = jListPerros.getSelectedIndex();
            VentanaEditarPerro vent = new VentanaEditarPerro(modelo, indice);
            vent.setVisible(true);
            vent.setLocationRelativeTo(null);
            dispose();
        }else{
            JOptionPane.showMessageDialog(rootPane, 
                    "Debe seleccionar un perro para poder editarlo");
        }
    }//GEN-LAST:event_jBtnEditarActionPerformed
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAgregar;
    private javax.swing.JButton jBtnBorrar;
    private javax.swing.JButton jBtnEditar;
    private javax.swing.JButton jButtonVolver;
    private javax.swing.JList jListPerros;
    private javax.swing.JPanel jPanelPerros;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
