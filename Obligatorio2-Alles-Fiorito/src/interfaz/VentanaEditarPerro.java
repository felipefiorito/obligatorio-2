package interfaz;

import dominio.Sistema;
import java.awt.Image;
import javax.swing.*;

public class VentanaEditarPerro extends javax.swing.JFrame {

    private Sistema modelo;
    private String imagen;
    private final int indice;

    public VentanaEditarPerro(Sistema unSistema, int indicePerro) {
        initComponents();
        setModelo(unSistema);
        indice = indicePerro;
        cargarTextos();
        cargarImagen();
    }

    public Sistema getModelo() {
        return modelo;
    }

    public void setModelo(Sistema unModelo) {
        this.modelo = unModelo;
    }

    public void cargarTextos() {
        imagen = modelo.getPerro(indice).getDireccionImagen();
        jTextFieldNombre.setText(modelo.getPerro(indice).getNombre());
        jTextFieldEdad.setText(Float.toString(modelo.getPerro(indice).getEdad()));
        jTextFieldRaza.setText(modelo.getPerro(indice).getRaza());
        jTextFieldAltura.setText(Float.toString(modelo.getPerro(indice).getAltura()));
        jTextFieldPeso.setText(Float.toString(modelo.getPerro(indice).getPeso()));
        jTextFieldComentario.setText(modelo.getPerro(indice).getComentarios());
    }

    public void cargarImagen() {
        ImageIcon img = new ImageIcon(imagen);
        Icon icono = new ImageIcon(img.getImage().getScaledInstance
            (jLabelImagen.getWidth(), jLabelImagen.getHeight(),Image.SCALE_DEFAULT));
        jLabelImagen.setIcon(icono);
        this.repaint();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelAgregPerro = new javax.swing.JPanel();
        jLabelEdad = new javax.swing.JLabel();
        jLabelNombre1 = new javax.swing.JLabel();
        jLabelRaza = new javax.swing.JLabel();
        jLabelAltura = new javax.swing.JLabel();
        jLabelPeso = new javax.swing.JLabel();
        jLabelPeso1 = new javax.swing.JLabel();
        jTextFieldNombre = new javax.swing.JTextField();
        jTextFieldAltura = new javax.swing.JTextField();
        jTextFieldComentario = new javax.swing.JTextField();
        jButtonEditar = new javax.swing.JButton();
        jButtonVolver = new javax.swing.JButton();
        jTextFieldEdad = new javax.swing.JTextField();
        jTextFieldRaza = new javax.swing.JTextField();
        jTextFieldPeso = new javax.swing.JTextField();
        jButtoCambiarImagen = new javax.swing.JButton();
        jLabelImagen = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelAgregPerro.setBackground(new java.awt.Color(143, 0, 0));

        jLabelEdad.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelEdad.setForeground(new java.awt.Color(255, 255, 204));
        jLabelEdad.setText("Edad ");

        jLabelNombre1.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelNombre1.setForeground(new java.awt.Color(255, 255, 204));
        jLabelNombre1.setText("Nombre");

        jLabelRaza.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelRaza.setForeground(new java.awt.Color(255, 255, 204));
        jLabelRaza.setText("Raza");

        jLabelAltura.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelAltura.setForeground(new java.awt.Color(255, 255, 204));
        jLabelAltura.setText("Altura");

        jLabelPeso.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelPeso.setForeground(new java.awt.Color(255, 255, 204));
        jLabelPeso.setText("Peso (kgs)");

        jLabelPeso1.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelPeso1.setForeground(new java.awt.Color(255, 255, 204));
        jLabelPeso1.setText("Comentario");

        jTextFieldNombre.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N

        jTextFieldAltura.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N

        jTextFieldComentario.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N

        jButtonEditar.setText("Editar");
        jButtonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditarActionPerformed(evt);
            }
        });

        jButtonVolver.setText("Volver");
        jButtonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverActionPerformed(evt);
            }
        });

        jTextFieldEdad.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N

        jTextFieldRaza.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N

        jTextFieldPeso.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N

        jButtoCambiarImagen.setText("Cambiar Imagen");
        jButtoCambiarImagen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtoCambiarImagenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelAgregPerroLayout = new javax.swing.GroupLayout(jPanelAgregPerro);
        jPanelAgregPerro.setLayout(jPanelAgregPerroLayout);
        jPanelAgregPerroLayout.setHorizontalGroup(
            jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                        .addComponent(jButtoCambiarImagen)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 117, Short.MAX_VALUE)
                        .addComponent(jLabelImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelAgregPerroLayout.createSequentialGroup()
                        .addComponent(jButtonVolver)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonEditar))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelAgregPerroLayout.createSequentialGroup()
                        .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelEdad, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabelPeso1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelAltura, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelRaza, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldAltura, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldNombre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldEdad, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldRaza, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldPeso, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldComentario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(63, 63, 63))
        );
        jPanelAgregPerroLayout.setVerticalGroup(
            jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelEdad, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldEdad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelRaza, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldRaza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelAltura, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldAltura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPeso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelPeso1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jTextFieldComentario, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                        .addComponent(jButtoCambiarImagen)
                        .addGap(49, 49, 49))
                    .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabelImagen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)))
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonVolver)
                    .addComponent(jButtonEditar))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelAgregPerro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelAgregPerro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarActionPerformed
        try {
            boolean ok = true;
            String nombre = jTextFieldNombre.getText();
            if(nombre.isEmpty()) {
                ok = false;
                JOptionPane.showMessageDialog(rootPane,
                         "Debe ingresar un nombre");
            }
            float edad = Float.parseFloat(jTextFieldEdad.getText());
            if(!this.getModelo().edadPermitida(edad)){
                ok = false;
                JOptionPane.showMessageDialog(rootPane,
                        "La edad ingresada no es válida,"
                                + " debe ser un número entre 0 y 40");
            }
            String raza = jTextFieldRaza.getText();
            if(raza.isEmpty()) {
                ok = false;
                JOptionPane.showMessageDialog(rootPane,
                        "Debe ingresar una raza");
            }
            float altura = Float.parseFloat(jTextFieldAltura.getText());
            if(!this.getModelo().alturaPermitida(altura)){
                ok = false;
                JOptionPane.showMessageDialog(rootPane,
                        "La altura ingresada no es válida,"
                                + " debe ser un número entre 5 y 200");
            }
            float peso = Float.parseFloat(jTextFieldPeso.getText());
            if(!this.getModelo().pesoPermitido(peso)){
                ok = false;
                JOptionPane.showMessageDialog(rootPane,
                        "El peso ingresado no es válido,"
                                + " debe ser un número entre 0.3 y 150");
            }
            String comentario = jTextFieldComentario.getText();
            String direccionImagen = imagen;
            
            if(!(modelo.getPerro(indice).getNombre().equals(nombre))) {
                if(modelo.existeNombrePerro(nombre)) {
                    JOptionPane.showMessageDialog(this, "Ya existe un"
                        + " perro con ese nombre",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    ok = false;
                }
            }
            if(ok) {
                this.getModelo().getPerro(indice).setNombre(nombre);
                this.getModelo().getPerro(indice).setEdad(edad);
                this.getModelo().getPerro(indice).setRaza(raza);
                this.getModelo().getPerro(indice).setAltura(altura);
                this.getModelo().getPerro(indice).setPeso(peso);
                this.getModelo().getPerro(indice).setComentarios(comentario);
                this.getModelo().getPerro(indice).setDireccionImagen(direccionImagen);
                VentanaMenuPerros vent = new VentanaMenuPerros(modelo);
                vent.setVisible(true);
                vent.setLocationRelativeTo(null);
                dispose();
            }
        }catch(NumberFormatException err) {
            JOptionPane.showMessageDialog(this,
                    "Se ingreso por lo menos un dato vacío o no númerico"
                    + " en un campo en el que se pide solo números",
                     "Error", JOptionPane.ERROR_MESSAGE);

        }catch(Exception err) {
            JOptionPane.showMessageDialog(this, "Error " + err.getMessage(),
                     "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButtonEditarActionPerformed

    private void jButtonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverActionPerformed
        VentanaMenuPerros vent = new VentanaMenuPerros(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonVolverActionPerformed

    private void jButtoCambiarImagenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtoCambiarImagenActionPerformed
        JFileChooser elegirArchivo = new JFileChooser();
        int resultado = elegirArchivo.showOpenDialog(null);
        if (resultado == (JFileChooser.APPROVE_OPTION)) {
            String direccion = elegirArchivo.getSelectedFile().getPath();
            imagen = direccion;
            cargarImagen();
        }
    }//GEN-LAST:event_jButtoCambiarImagenActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtoCambiarImagen;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonVolver;
    private javax.swing.JLabel jLabelAltura;
    private javax.swing.JLabel jLabelEdad;
    private javax.swing.JLabel jLabelImagen;
    private javax.swing.JLabel jLabelNombre1;
    private javax.swing.JLabel jLabelPeso;
    private javax.swing.JLabel jLabelPeso1;
    private javax.swing.JLabel jLabelRaza;
    private javax.swing.JPanel jPanelAgregPerro;
    private javax.swing.JTextField jTextFieldAltura;
    private javax.swing.JTextField jTextFieldComentario;
    private javax.swing.JTextField jTextFieldEdad;
    private javax.swing.JTextField jTextFieldNombre;
    private javax.swing.JTextField jTextFieldPeso;
    private javax.swing.JTextField jTextFieldRaza;
    // End of variables declaration//GEN-END:variables
}
