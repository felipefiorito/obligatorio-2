
package interfaz;

import dominio.Sistema;
import java.util.Date;
import javax.swing.*;

public class VentanaRegistrarActividad extends javax.swing.JFrame {

    private Sistema modelo;
    
    public VentanaRegistrarActividad(Sistema unSistema) {
        initComponents();
        setModelo(unSistema);
        cargarListaPersonas();
        cargarListaPerros();
        jPanelOpcionesPaseo.setVisible(false);
    }
    
    public Sistema getModelo(){
        return modelo;
    }
    
    public void setModelo(Sistema unModelo){
        this.modelo =unModelo;
    }
    
    public void cargarListaPersonas() {
        String[] personas = new String[modelo.getListaPersonas().size()];
        jComboBoxPersonas.removeAll();
        for (int i = 0; i < modelo.getListaPersonas().size(); i++) {
            personas[i] = modelo.getListaPersonas().get(i).getNombre();
        }
        jComboBoxPersonas.setModel(new javax.swing.DefaultComboBoxModel<>(personas));
    }
    
    public void cargarListaPerros() {
        String[] perros = new String[modelo.getListaPerros().size()];
        jComboBoxPerros.removeAll();
        for (int i = 0; i < modelo.getListaPerros().size(); i++) {
            perros[i] = modelo.getListaPerros().get(i).getNombre();
        }
        jComboBoxPerros.setModel(new javax.swing.DefaultComboBoxModel<>(perros));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabelPerro = new javax.swing.JLabel();
        jComboBoxPerros = new javax.swing.JComboBox<>();
        jLabelDia = new javax.swing.JLabel();
        jDateChooserPaseo = new com.toedter.calendar.JDateChooser();
        jLabelResponsable = new javax.swing.JLabel();
        jTextFieldHoras = new javax.swing.JTextField();
        jLabelDospts = new javax.swing.JLabel();
        jTextFieldMinutos = new javax.swing.JTextField();
        jLabelHorario = new javax.swing.JLabel();
        jLabelDospts1 = new javax.swing.JLabel();
        jLabelHorario2 = new javax.swing.JLabel();
        jLabelIngreseHorario = new javax.swing.JLabel();
        jComboBoxPersonas = new javax.swing.JComboBox<>();
        jLabelResponsable1 = new javax.swing.JLabel();
        JTextFieldTipoActividad = new javax.swing.JTextField();
        jPanelOpcionesPaseo = new javax.swing.JPanel();
        jLabelDireccionOrigen = new javax.swing.JLabel();
        jLabelDireccionDestino = new javax.swing.JLabel();
        JTextFieldDireccionOrigen = new javax.swing.JTextField();
        JTextFieldDireccionDestino = new javax.swing.JTextField();
        jButtonMapa = new javax.swing.JButton();
        jButtonVolver = new javax.swing.JButton();
        jButtonRegistrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(140, 0, 0));

        jLabelPerro.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelPerro.setForeground(new java.awt.Color(255, 255, 204));
        jLabelPerro.setText("Seleccione un/a perro/a");

        jComboBoxPerros.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jComboBoxPerros.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabelDia.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelDia.setForeground(new java.awt.Color(255, 255, 204));
        jLabelDia.setText("Seleccione un día");

        jLabelResponsable.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelResponsable.setForeground(new java.awt.Color(255, 255, 204));
        jLabelResponsable.setText("Seleccione un responsable");

        jTextFieldHoras.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jTextFieldHoras.setText("12");

        jLabelDospts.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelDospts.setForeground(new java.awt.Color(255, 255, 204));
        jLabelDospts.setText(":");

        jTextFieldMinutos.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jTextFieldMinutos.setText("00");

        jLabelHorario.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelHorario.setForeground(new java.awt.Color(255, 255, 204));
        jLabelHorario.setText("HH");

        jLabelDospts1.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelDospts1.setForeground(new java.awt.Color(255, 255, 204));
        jLabelDospts1.setText(":");

        jLabelHorario2.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelHorario2.setForeground(new java.awt.Color(255, 255, 204));
        jLabelHorario2.setText("MM");

        jLabelIngreseHorario.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelIngreseHorario.setForeground(new java.awt.Color(255, 255, 204));
        jLabelIngreseHorario.setText("Ingrese un horario");

        jComboBoxPersonas.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jComboBoxPersonas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabelResponsable1.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelResponsable1.setForeground(new java.awt.Color(255, 255, 204));
        jLabelResponsable1.setText("Elija tipo de actividad realizada");

        JTextFieldTipoActividad.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        JTextFieldTipoActividad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                JTextFieldTipoActividadKeyReleased(evt);
            }
        });

        jPanelOpcionesPaseo.setBackground(new java.awt.Color(140, 0, 0));

        jLabelDireccionOrigen.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelDireccionOrigen.setForeground(new java.awt.Color(255, 255, 204));
        jLabelDireccionOrigen.setText("Ingrese direccion de origen");

        jLabelDireccionDestino.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelDireccionDestino.setForeground(new java.awt.Color(255, 255, 204));
        jLabelDireccionDestino.setText("Ingrese direccion de destino");

        JTextFieldDireccionOrigen.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N

        JTextFieldDireccionDestino.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N

        jButtonMapa.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonMapa.setText("Ver en Mapa");
        jButtonMapa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonMapaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelOpcionesPaseoLayout = new javax.swing.GroupLayout(jPanelOpcionesPaseo);
        jPanelOpcionesPaseo.setLayout(jPanelOpcionesPaseoLayout);
        jPanelOpcionesPaseoLayout.setHorizontalGroup(
            jPanelOpcionesPaseoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelOpcionesPaseoLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanelOpcionesPaseoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelOpcionesPaseoLayout.createSequentialGroup()
                        .addComponent(jLabelDireccionOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(JTextFieldDireccionOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelOpcionesPaseoLayout.createSequentialGroup()
                        .addComponent(jLabelDireccionDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(263, 263, 263)
                        .addComponent(JTextFieldDireccionDestino, javax.swing.GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE))
                    .addGroup(jPanelOpcionesPaseoLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonMapa)))
                .addGap(43, 43, 43))
        );
        jPanelOpcionesPaseoLayout.setVerticalGroup(
            jPanelOpcionesPaseoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelOpcionesPaseoLayout.createSequentialGroup()
                .addGroup(jPanelOpcionesPaseoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelDireccionOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTextFieldDireccionOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelOpcionesPaseoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelDireccionDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTextFieldDireccionDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addComponent(jButtonMapa, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jButtonVolver.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonVolver.setText("Volver");
        jButtonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverActionPerformed(evt);
            }
        });

        jButtonRegistrar.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonRegistrar.setText("Registrar");
        jButtonRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRegistrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelPerro, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelDia, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelResponsable, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelIngreseHorario, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelResponsable1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(10, 10, 10)
                            .addComponent(jLabelHorario)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jLabelDospts1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabelHorario2))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jDateChooserPaseo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBoxPerros, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jTextFieldHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(7, 7, 7)
                            .addComponent(jLabelDospts, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jComboBoxPersonas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTextFieldTipoActividad, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44))
            .addComponent(jPanelOpcionesPaseo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jButtonVolver)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonRegistrar)
                .addGap(43, 43, 43))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jComboBoxPerros))
                    .addComponent(jLabelPerro, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelDia, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDateChooserPaseo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldMinutos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextFieldHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelDospts)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelHorario)
                            .addComponent(jLabelDospts1)
                            .addComponent(jLabelHorario2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBoxPersonas, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelIngreseHorario, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(jLabelResponsable, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelResponsable1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTextFieldTipoActividad, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanelOpcionesPaseo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonVolver)
                    .addComponent(jButtonRegistrar))
                .addGap(26, 26, 26))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverActionPerformed
        VentanaActividades vent = new VentanaActividades(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonVolverActionPerformed

    private void jButtonRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRegistrarActionPerformed
        try {
            if(this.getModelo().getListaPerros().isEmpty() 
                || this.getModelo().getListaPersonas().isEmpty()){
                JOptionPane.showMessageDialog(rootPane,
                        "No se puede registrar una actividad hasta "
                            + "no haber registrado por lo menos una persona"
                                + " y un perro");
            }else{
                boolean ok = true;
                int horas = Integer.parseInt(jTextFieldHoras.getText());
                int minutos = Integer.parseInt(jTextFieldMinutos.getText());
                if (!this.getModelo().horaPermitida(horas)
                        || !this.getModelo().minutosPermitida(minutos)
                        || jTextFieldHoras.getText().isEmpty()
                        || jTextFieldMinutos.getText().isEmpty()) {
                    ok = false;
                    JOptionPane.showMessageDialog(this,
                            "La hora ingresada no es correcta",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
                String hora = jTextFieldHoras.getText() + ":"
                        + jTextFieldMinutos.getText();
                Date fecha = jDateChooserPaseo.getDate();
                if(fecha == null){
                    JOptionPane.showMessageDialog(rootPane,
                            "Debe ingresar una fecha");
                    ok = false;
                }
                String tipoActividad = JTextFieldTipoActividad.getText();
                if(tipoActividad.isEmpty()){
                    ok=false;
                    JOptionPane.showMessageDialog(rootPane,
                            "Debe ingresar un tipo de actividad realizada");
                }
                if(ok){
                    if(jPanelOpcionesPaseo.isVisible()){
                        String mapaPaseo = JTextFieldDireccionOrigen.getText() + "/"
                                + JTextFieldDireccionDestino.getText();
                        this.getModelo().registrarPaseoRealizado(
                                jComboBoxPerros.getSelectedIndex(),
                                    jComboBoxPersonas.getSelectedIndex(),
                                        fecha, hora, tipoActividad,mapaPaseo);
                        JOptionPane.showMessageDialog(this, "Paseo realizado"
                                + " registrado con éxito",
                                    "Éxito", JOptionPane.OK_OPTION);
                        VentanaActividades vent = new VentanaActividades(modelo);
                        vent.setVisible(true);
                        vent.setLocationRelativeTo(null);
                        dispose();
                    }else{
                        this.getModelo().registrarActividadRealizada(
                                jComboBoxPerros.getSelectedIndex(),
                                    jComboBoxPersonas.getSelectedIndex(),
                                        fecha, hora, tipoActividad);
                        JOptionPane.showMessageDialog(this, "Actividad realizada"
                                + " registrado con éxito",
                                    "Éxito", JOptionPane.OK_OPTION);
                        VentanaActividades vent = new VentanaActividades(modelo);
                        vent.setVisible(true);
                        vent.setLocationRelativeTo(null);
                        dispose();
                    }
                }
            }
        } catch (NumberFormatException err) {
            JOptionPane.showMessageDialog(this,
                "Se ingreso por lo menos un dato vacío o no númerico"
                + " en un campo en el que se pide solo números"
                , "Error", JOptionPane.ERROR_MESSAGE);
        } 
    }//GEN-LAST:event_jButtonRegistrarActionPerformed

    private void JTextFieldTipoActividadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTextFieldTipoActividadKeyReleased
        if(JTextFieldTipoActividad.getText().equalsIgnoreCase("Paseo")){
            jPanelOpcionesPaseo.setVisible(true);
        }else{
            jPanelOpcionesPaseo.setVisible(false);
        }
    }//GEN-LAST:event_JTextFieldTipoActividadKeyReleased

    private void jButtonMapaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonMapaActionPerformed
        String origen = JTextFieldDireccionOrigen.getText();
        String destino ="/"+ JTextFieldDireccionDestino.getText();
        if(destino.isEmpty() || origen.isEmpty()){
            JOptionPane.showMessageDialog(rootPane, "Debe ingresar un origen"
                + " y un destino para ver en el mapa");
        }else{
            String direccion=origen+destino;
            VentanaMapa vent = new VentanaMapa(direccion);
            vent.setLocationRelativeTo(null);
            vent.setVisible(true);
        }
    }//GEN-LAST:event_jButtonMapaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField JTextFieldDireccionDestino;
    private javax.swing.JTextField JTextFieldDireccionOrigen;
    private javax.swing.JTextField JTextFieldTipoActividad;
    private javax.swing.JButton jButtonMapa;
    private javax.swing.JButton jButtonRegistrar;
    private javax.swing.JButton jButtonVolver;
    private javax.swing.JComboBox<String> jComboBoxPerros;
    private javax.swing.JComboBox<String> jComboBoxPersonas;
    private com.toedter.calendar.JDateChooser jDateChooserPaseo;
    private javax.swing.JLabel jLabelDia;
    private javax.swing.JLabel jLabelDireccionDestino;
    private javax.swing.JLabel jLabelDireccionOrigen;
    private javax.swing.JLabel jLabelDospts;
    private javax.swing.JLabel jLabelDospts1;
    private javax.swing.JLabel jLabelHorario;
    private javax.swing.JLabel jLabelHorario2;
    private javax.swing.JLabel jLabelIngreseHorario;
    private javax.swing.JLabel jLabelPerro;
    private javax.swing.JLabel jLabelResponsable;
    private javax.swing.JLabel jLabelResponsable1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelOpcionesPaseo;
    private javax.swing.JTextField jTextFieldHoras;
    private javax.swing.JTextField jTextFieldMinutos;
    // End of variables declaration//GEN-END:variables
}
