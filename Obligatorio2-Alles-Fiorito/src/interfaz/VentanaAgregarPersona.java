
package interfaz;

import dominio.Sistema;
import java.awt.Image;
import java.io.File;
import javax.swing.*;

public class VentanaAgregarPersona extends javax.swing.JFrame {

    private Sistema modelo;
    private String imagen;
    
    public VentanaAgregarPersona(Sistema unSistema){
        initComponents();
        setModelo(unSistema);
        imagen= new File("imagenes/persona.png").getPath();
        cargarImagen();
    }

    public Sistema getModelo(){
        return modelo;
    }
    
    public void setModelo(Sistema unModelo){
        this.modelo =unModelo;
    }
    
    public void cargarImagen(){
        ImageIcon img = new ImageIcon(imagen);
        Icon icono = new ImageIcon(img.getImage().getScaledInstance
            (jLabelImagen.getWidth(), jLabelImagen.getHeight(),
                Image.SCALE_DEFAULT));
        jLabelImagen.setIcon(icono);
        this.repaint();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelAgregPerro = new javax.swing.JPanel();
        jLabelMail = new javax.swing.JLabel();
        jLabelNombre1 = new javax.swing.JLabel();
        jTextFieldNombre = new javax.swing.JTextField();
        jButtonAgregar = new javax.swing.JButton();
        jButtonVolver = new javax.swing.JButton();
        jTextFieldMail = new javax.swing.JTextField();
        jButtoCambiarImagen = new javax.swing.JButton();
        jLabelImagen = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelAgregPerro.setBackground(new java.awt.Color(143, 0, 0));
        jPanelAgregPerro.setPreferredSize(new java.awt.Dimension(400, 400));

        jLabelMail.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelMail.setForeground(new java.awt.Color(255, 255, 204));
        jLabelMail.setText("Mail");

        jLabelNombre1.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelNombre1.setForeground(new java.awt.Color(255, 255, 204));
        jLabelNombre1.setText("Nombre");

        jTextFieldNombre.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N

        jButtonAgregar.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonAgregar.setText("Agregar");
        jButtonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgregarActionPerformed(evt);
            }
        });

        jButtonVolver.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonVolver.setText("Volver");
        jButtonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverActionPerformed(evt);
            }
        });

        jTextFieldMail.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N

        jButtoCambiarImagen.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtoCambiarImagen.setText("Cambiar Imagen");
        jButtoCambiarImagen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtoCambiarImagenActionPerformed(evt);
            }
        });

        jLabelImagen.setOpaque(true);

        javax.swing.GroupLayout jPanelAgregPerroLayout = new javax.swing.GroupLayout(jPanelAgregPerro);
        jPanelAgregPerro.setLayout(jPanelAgregPerroLayout);
        jPanelAgregPerroLayout.setHorizontalGroup(
            jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelAgregPerroLayout.createSequentialGroup()
                        .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabelNombre1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(jLabelMail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldMail, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
                            .addComponent(jTextFieldNombre)))
                    .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                        .addComponent(jButtoCambiarImagen)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 87, Short.MAX_VALUE)
                        .addComponent(jLabelImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                        .addComponent(jButtonVolver)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonAgregar)))
                .addGap(63, 63, 63))
        );
        jPanelAgregPerroLayout.setVerticalGroup(
            jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelMail, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabelImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 64, Short.MAX_VALUE)
                        .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonVolver)
                            .addComponent(jButtonAgregar))
                        .addGap(53, 53, 53))
                    .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(jButtoCambiarImagen)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelAgregPerro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelAgregPerro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgregarActionPerformed
       try {
            boolean ok = true;
            String nombre = jTextFieldNombre.getText();
            if(nombre.isEmpty()){
                ok = false;
                JOptionPane.showMessageDialog(rootPane
                        , "Debe ingresar un nombre");
            }
            String direccionImagen = imagen;
            String mail = jTextFieldMail.getText();
            if(mail.isEmpty()){
                JOptionPane.showMessageDialog(rootPane
                        , "Debe ingresar un mail");
            }else if(!this.getModelo().mailPermitido(mail)){
                JOptionPane.showMessageDialog(rootPane
                        , "El mail ingresado no es válido");
            }else if(modelo.existeMailPersona(mail)){
                JOptionPane.showMessageDialog
                    (this, "Ya existe una persona con ese mail",
                        "Error", JOptionPane.ERROR_MESSAGE);
            }else if(ok){
                this.getModelo().agregarUnaPersona(nombre, mail, direccionImagen);
                JOptionPane.showMessageDialog(rootPane
                        , "Persona agregada con éxito");
                VentanaMenuPersonas vent = new VentanaMenuPersonas(modelo);
                vent.setVisible(true);
                vent.setLocationRelativeTo(null);
                dispose();
            }  
        } catch (NumberFormatException err) {
            JOptionPane.showMessageDialog(this,
                    "Se ingreso por lo menos un dato vacío o no númerico"
                            + " en un campo en el que se pide solo números"
                                , "Error", JOptionPane.ERROR_MESSAGE);

        } catch(Exception err) {
            JOptionPane.showMessageDialog(this, "Error " + err.getMessage() 
                    ,"Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButtonAgregarActionPerformed

    private void jButtonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverActionPerformed
        VentanaMenuPersonas vent = new VentanaMenuPersonas(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonVolverActionPerformed

    private void jButtoCambiarImagenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtoCambiarImagenActionPerformed
        JFileChooser elegirArchivo = new JFileChooser();
        int resultado = elegirArchivo.showOpenDialog(null);
        if(resultado == (JFileChooser.APPROVE_OPTION)){
            String direccion = elegirArchivo.getSelectedFile().getPath();
            imagen= direccion;
            cargarImagen();
        }
    }//GEN-LAST:event_jButtoCambiarImagenActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtoCambiarImagen;
    private javax.swing.JButton jButtonAgregar;
    private javax.swing.JButton jButtonVolver;
    private javax.swing.JLabel jLabelImagen;
    private javax.swing.JLabel jLabelMail;
    private javax.swing.JLabel jLabelNombre1;
    private javax.swing.JPanel jPanelAgregPerro;
    private javax.swing.JTextField jTextFieldMail;
    private javax.swing.JTextField jTextFieldNombre;
    // End of variables declaration//GEN-END:variables
}
