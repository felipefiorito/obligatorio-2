
package interfaz;

import javax.swing.*;
import dominio.Sistema;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class VentanaNotificaciones extends javax.swing.JFrame {

    private Sistema modelo;
    
    public VentanaNotificaciones(Sistema unSistema) {
        initComponents();
        setModelo(unSistema);
        cargarLista();
    }

    public Sistema getModelo(){
        return modelo;
    }
    
    public void setModelo(Sistema unModelo){
        this.modelo = unModelo;
    }
    
    public void cargarLista(){
        jListPersonas.removeAll();
        DefaultListModel modeloLista = new DefaultListModel();
        for(int i=0; i<modelo.getListaPersonas().size();i++){
            modeloLista.addElement(modelo.getPersona(i));        
        }
        jListPersonas.setModel(modeloLista);
        jListPersonas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    public void enviarMail(String mensaje) {          
        Properties props = new Properties();
        props.put("mail.smtp.host", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("notificaciondogapi@gmail.com",
                    "dogapi123");
            }
        });
        try {
            MimeMessage msg = new MimeMessage(session);
            String direccionMail = this.getModelo().getPersona
                (jListPersonas.getSelectedIndex()).getMail();
            String to = direccionMail;
            InternetAddress[] address = InternetAddress.parse(to, true);
            msg.setFrom("notificaciondogapi@gmail.com");
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject("Notificacion Dog App sobre actividades pendientes");
            msg.setSentDate(new Date());
            msg.setText(mensaje);
            msg.setHeader("XPriority", "1");
            Transport.send(msg);
            JOptionPane.showMessageDialog(rootPane, 
                "El mail se ha enviado correctamente");
        } catch (MessagingException mex) {
            JOptionPane.showMessageDialog(rootPane, 
                "No se pudo enviar el mail");
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelAgregPerro = new javax.swing.JPanel();
        jLabelVerNotificaciones = new javax.swing.JLabel();
        jButtonVolver = new javax.swing.JButton();
        jLabelEnviarNotificaciones = new javax.swing.JLabel();
        jButtonEnviar = new javax.swing.JButton();
        jButtonVer = new javax.swing.JButton();
        jLabelLista = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListPersonas = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelAgregPerro.setBackground(new java.awt.Color(143, 0, 0));
        jPanelAgregPerro.setPreferredSize(new java.awt.Dimension(400, 400));

        jLabelVerNotificaciones.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelVerNotificaciones.setForeground(new java.awt.Color(255, 255, 204));
        jLabelVerNotificaciones.setText("Ver Notificaciones");

        jButtonVolver.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonVolver.setText("Volver");
        jButtonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverActionPerformed(evt);
            }
        });

        jLabelEnviarNotificaciones.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelEnviarNotificaciones.setForeground(new java.awt.Color(255, 255, 204));
        jLabelEnviarNotificaciones.setText("Enviar notificaciones por mail");

        jButtonEnviar.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonEnviar.setText("Enviar");
        jButtonEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEnviarActionPerformed(evt);
            }
        });

        jButtonVer.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonVer.setText("Ver");
        jButtonVer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVerActionPerformed(evt);
            }
        });

        jLabelLista.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelLista.setForeground(new java.awt.Color(255, 255, 204));
        jLabelLista.setText("Lista Personas");

        jListPersonas.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jScrollPane2.setViewportView(jListPersonas);

        javax.swing.GroupLayout jPanelAgregPerroLayout = new javax.swing.GroupLayout(jPanelAgregPerro);
        jPanelAgregPerro.setLayout(jPanelAgregPerroLayout);
        jPanelAgregPerroLayout.setHorizontalGroup(
            jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                        .addComponent(jButtonVolver)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelAgregPerroLayout.createSequentialGroup()
                        .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelEnviarNotificaciones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelVerNotificaciones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButtonEnviar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonVer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(57, 57, 57)
                        .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelLista, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(22, 22, 22))))
        );
        jPanelAgregPerroLayout.setVerticalGroup(
            jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelVerNotificaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonVer))
                .addGap(18, 18, 18)
                .addGroup(jPanelAgregPerroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelEnviarNotificaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonEnviar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 123, Short.MAX_VALUE)
                .addComponent(jButtonVolver)
                .addGap(53, 53, 53))
            .addGroup(jPanelAgregPerroLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabelLista, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelAgregPerro, javax.swing.GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelAgregPerro, javax.swing.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverActionPerformed
        VentanaPrincipal vent = new VentanaPrincipal(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonVolverActionPerformed

    private void jButtonVerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVerActionPerformed
        if(jListPersonas.getSelectedIndex()!=-1){
            int cantidad = this.getModelo().cantidadNotificaciones
                (this.getModelo().getListaActividades(),
                    this.getModelo().getPersona
                        (jListPersonas.getSelectedIndex()));
            if(cantidad==0){
                JOptionPane.showMessageDialog(rootPane, 
                    "Esta persona no tiene  ninguna actividad pendiente");
            }else if(cantidad==1){
                String fechas = this.getModelo().fechasNotificaciones
                    (this.getModelo().getListaActividades(),
                        this.getModelo().getPersona
                            (jListPersonas.getSelectedIndex()));
                JOptionPane.showMessageDialog(rootPane, 
                    "Esta persona tiene " + cantidad + " actividad pendiente\n"
                            + "En el día: " + fechas);             
            }else{
                String fechas = this.getModelo().fechasNotificaciones
                    (this.getModelo().getListaActividades(),
                        this.getModelo().getPersona
                            (jListPersonas.getSelectedIndex()));
                JOptionPane.showMessageDialog(rootPane, 
                    "Esta persona tiene " + cantidad 
                        + " actividades pendientes\n" + "En los días: \n" + fechas);
            }
        }else{
            JOptionPane.showMessageDialog(rootPane, 
                    "Debe seleccionar una persona para poder ver sus notificaciones");
        }
    }//GEN-LAST:event_jButtonVerActionPerformed

    private void jButtonEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEnviarActionPerformed
        if(jListPersonas.getSelectedIndex()!=-1){
            String mensaje;
            int cantidad = this.getModelo().cantidadNotificaciones
                (this.getModelo().getListaActividades(),
                    this.getModelo().getPersona
                        (jListPersonas.getSelectedIndex()));
            if(cantidad==0){
                JOptionPane.showMessageDialog(rootPane, 
                    "Usted no tiene ninguna actividad pendiente");
            }else if(cantidad==1){
                String fechas = this.getModelo().fechasNotificaciones
                    (this.getModelo().getListaActividades(),
                        this.getModelo().getPersona
                            (jListPersonas.getSelectedIndex()));
                mensaje = "Usted tiene " + cantidad + " actividad pendiente\n"
                            + "En el día: " + fechas;
                enviarMail(mensaje);            
            }else{
                String fechas = this.getModelo().fechasNotificaciones
                    (this.getModelo().getListaActividades(),
                        this.getModelo().getPersona
                            (jListPersonas.getSelectedIndex()));
                mensaje = "Usted tiene " + cantidad + " actividades pendientes\n"
                            + "En los días: \n" + fechas;
                enviarMail(mensaje); 
            }
        }else{
            JOptionPane.showMessageDialog(rootPane, 
                    "Debe seleccionar una persona para poder ver sus notificaciones");
        }
    }//GEN-LAST:event_jButtonEnviarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonEnviar;
    private javax.swing.JButton jButtonVer;
    private javax.swing.JButton jButtonVolver;
    private javax.swing.JLabel jLabelEnviarNotificaciones;
    private javax.swing.JLabel jLabelLista;
    private javax.swing.JLabel jLabelVerNotificaciones;
    private javax.swing.JList jListPersonas;
    private javax.swing.JPanel jPanelAgregPerro;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
