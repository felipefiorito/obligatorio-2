
package interfaz;

import dominio.Sistema;

public class VentanaPrincipal extends javax.swing.JFrame {

    private Sistema modelo;

    public VentanaPrincipal(Sistema unSistema) {
        initComponents();
        setModelo(unSistema);
    }

    public Sistema getModelo(){
        return modelo;
    }
    
    public void setModelo(Sistema unModelo){
        this.modelo =unModelo;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelPrincipal = new javax.swing.JPanel();
        jButtonPerros = new javax.swing.JButton();
        jButtonActividades = new javax.swing.JButton();
        jButtonPersonas = new javax.swing.JButton();
        jButtonCalendario = new javax.swing.JButton();
        jButtonNotificaciones = new javax.swing.JButton();
        jButtonCalendarioRegistradas = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelPrincipal.setBackground(new java.awt.Color(143, 0, 0));

        jButtonPerros.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonPerros.setText("Mis Perros");
        jButtonPerros.setMaximumSize(new java.awt.Dimension(105, 23));
        jButtonPerros.setMinimumSize(new java.awt.Dimension(105, 23));
        jButtonPerros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPerrosActionPerformed(evt);
            }
        });

        jButtonActividades.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonActividades.setText("Actividades");
        jButtonActividades.setMaximumSize(new java.awt.Dimension(105, 23));
        jButtonActividades.setMinimumSize(new java.awt.Dimension(105, 23));
        jButtonActividades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonActividadesActionPerformed(evt);
            }
        });

        jButtonPersonas.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonPersonas.setText("Personas");
        jButtonPersonas.setMaximumSize(new java.awt.Dimension(105, 23));
        jButtonPersonas.setMinimumSize(new java.awt.Dimension(105, 23));
        jButtonPersonas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPersonasActionPerformed(evt);
            }
        });

        jButtonCalendario.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonCalendario.setText("Calendario de Actividades Agendadas");
        jButtonCalendario.setMaximumSize(new java.awt.Dimension(105, 23));
        jButtonCalendario.setMinimumSize(new java.awt.Dimension(105, 23));
        jButtonCalendario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCalendarioActionPerformed(evt);
            }
        });

        jButtonNotificaciones.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonNotificaciones.setText("Notificaciones");
        jButtonNotificaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNotificacionesActionPerformed(evt);
            }
        });

        jButtonCalendarioRegistradas.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonCalendarioRegistradas.setText("Calendario de Actividades Registradas");
        jButtonCalendarioRegistradas.setMaximumSize(new java.awt.Dimension(105, 23));
        jButtonCalendarioRegistradas.setMinimumSize(new java.awt.Dimension(105, 23));
        jButtonCalendarioRegistradas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCalendarioRegistradasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelPrincipalLayout = new javax.swing.GroupLayout(jPanelPrincipal);
        jPanelPrincipal.setLayout(jPanelPrincipalLayout);
        jPanelPrincipalLayout.setHorizontalGroup(
            jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                .addGap(157, 157, 157)
                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonPerros, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonActividades, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonPersonas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonCalendario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonNotificaciones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonCalendarioRegistradas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(164, Short.MAX_VALUE))
        );
        jPanelPrincipalLayout.setVerticalGroup(
            jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addComponent(jButtonPerros, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonPersonas, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonActividades, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonCalendario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonCalendarioRegistradas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonNotificaciones)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonPerrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPerrosActionPerformed
       VentanaMenuPerros vent = new VentanaMenuPerros(modelo);
       vent.setVisible(true);
       vent.setLocationRelativeTo(null);
       dispose();
    }//GEN-LAST:event_jButtonPerrosActionPerformed

    private void jButtonActividadesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonActividadesActionPerformed
        VentanaActividades vent = new VentanaActividades(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonActividadesActionPerformed

    private void jButtonPersonasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPersonasActionPerformed
        VentanaMenuPersonas vent = new VentanaMenuPersonas(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonPersonasActionPerformed

    private void jButtonCalendarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCalendarioActionPerformed
        VentanaCalendarioAgendadas vent = new VentanaCalendarioAgendadas(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonCalendarioActionPerformed

    private void jButtonNotificacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNotificacionesActionPerformed
        VentanaNotificaciones vent = new VentanaNotificaciones(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonNotificacionesActionPerformed

    private void jButtonCalendarioRegistradasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCalendarioRegistradasActionPerformed
        VentanaCalendarioRegistradas vent = new VentanaCalendarioRegistradas(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonCalendarioRegistradasActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonActividades;
    private javax.swing.JButton jButtonCalendario;
    private javax.swing.JButton jButtonCalendarioRegistradas;
    private javax.swing.JButton jButtonNotificaciones;
    private javax.swing.JButton jButtonPerros;
    private javax.swing.JButton jButtonPersonas;
    private javax.swing.JPanel jPanelPrincipal;
    // End of variables declaration//GEN-END:variables
}
