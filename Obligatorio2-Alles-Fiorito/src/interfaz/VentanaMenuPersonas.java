
package interfaz;

import javax.swing.*;
import dominio.Sistema;
import java.awt.Image;

public class VentanaMenuPersonas extends javax.swing.JFrame {

    private Sistema modelo;
    private String imagen;
    
    public VentanaMenuPersonas(Sistema unSistema) {
        initComponents();
        setModelo(unSistema);
        cargarLista();
    }
    
    public Sistema getModelo(){
        return modelo;
    }
    
    public void setModelo(Sistema unModelo){
        this.modelo =unModelo;
    }
    
    public void cargarLista(){
        jListPersonas.removeAll();
        DefaultListModel modeloLista = new DefaultListModel();
        for(int i=0; i<modelo.getListaPersonas().size();i++){
            modeloLista.addElement(modelo.getPersona(i));
            
        }
        jListPersonas.setModel(modeloLista);
        jBtnEditar.setEnabled(modelo.hayPersona());
        jListPersonas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelPersonas = new javax.swing.JPanel();
        jBtnAgregar = new javax.swing.JButton();
        jBtnBorrar = new javax.swing.JButton();
        jBtnEditar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListPersonas = new javax.swing.JList();
        jButtonVolver = new javax.swing.JButton();
        jLabelImagen = new javax.swing.JLabel();
        jBtnVerImagen = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelPersonas.setBackground(new java.awt.Color(143, 0, 0));

        jBtnAgregar.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jBtnAgregar.setText("Agregar");
        jBtnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAgregarActionPerformed(evt);
            }
        });

        jBtnBorrar.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jBtnBorrar.setText("Borrar");
        jBtnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnBorrarActionPerformed(evt);
            }
        });

        jBtnEditar.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jBtnEditar.setText("Editar");
        jBtnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnEditarActionPerformed(evt);
            }
        });

        jListPersonas.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jScrollPane2.setViewportView(jListPersonas);

        jButtonVolver.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonVolver.setText("Volver");
        jButtonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverActionPerformed(evt);
            }
        });

        jLabelImagen.setOpaque(true);

        jBtnVerImagen.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jBtnVerImagen.setText("Ver imagen");
        jBtnVerImagen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVerImagenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelPersonasLayout = new javax.swing.GroupLayout(jPanelPersonas);
        jPanelPersonas.setLayout(jPanelPersonasLayout);
        jPanelPersonasLayout.setHorizontalGroup(
            jPanelPersonasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPersonasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelPersonasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonVolver, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBtnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBtnBorrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBtnAgregar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBtnVerImagen, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 133, Short.MAX_VALUE)
                .addGroup(jPanelPersonasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabelImagen, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(37, 37, 37))
        );
        jPanelPersonasLayout.setVerticalGroup(
            jPanelPersonasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPersonasLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(jPanelPersonasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPersonasLayout.createSequentialGroup()
                        .addComponent(jBtnAgregar)
                        .addGap(18, 18, 18)
                        .addComponent(jBtnBorrar)
                        .addGap(18, 18, 18)
                        .addComponent(jBtnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanelPersonasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPersonasLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabelImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelPersonasLayout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(jBtnVerImagen)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addComponent(jButtonVolver)
                .addGap(34, 34, 34))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPersonas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPersonas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAgregarActionPerformed
        VentanaAgregarPersona vent = new VentanaAgregarPersona(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jBtnAgregarActionPerformed

    private void jBtnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnBorrarActionPerformed
        if(jListPersonas.getSelectedIndex()!=-1){
            modelo.borrarPersona(modelo.getPersona
                (jListPersonas.getSelectedIndex()));
        }else{
            JOptionPane.showMessageDialog(rootPane, 
                    "Debe seleccionar una persona para poder borrarla");
        }
        cargarLista();
    }//GEN-LAST:event_jBtnBorrarActionPerformed

    private void jButtonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverActionPerformed
        VentanaPrincipal vent = new VentanaPrincipal(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonVolverActionPerformed

    private void jBtnVerImagenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVerImagenActionPerformed
        if(jListPersonas.getSelectedIndex()!=-1){
            imagen = modelo.getPersona
                (jListPersonas.getSelectedIndex()).getDireccionImagen();
            cargarImagen();
        }else{
            JOptionPane.showMessageDialog(rootPane, 
                    "Debe seleccionar una persona para poder ver su imagen");
        }
    }//GEN-LAST:event_jBtnVerImagenActionPerformed

    private void jBtnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnEditarActionPerformed
        if(jListPersonas.getSelectedIndex()!=-1){
            int indice = jListPersonas.getSelectedIndex();
            VentanaEditarPersona vent = new VentanaEditarPersona(modelo, indice);
            vent.setVisible(true);
            vent.setLocationRelativeTo(null);
            dispose();
        }else{
            JOptionPane.showMessageDialog(rootPane, 
                    "Debe seleccionar una persona para poder editarla");
        }
    }//GEN-LAST:event_jBtnEditarActionPerformed

    public void cargarImagen(){
        ImageIcon img = new ImageIcon(imagen);
        Icon icono = new ImageIcon(img.getImage().getScaledInstance
            (jLabelImagen.getWidth(), jLabelImagen.getHeight(),
                Image.SCALE_DEFAULT));
        jLabelImagen.setIcon(icono);
        this.repaint();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAgregar;
    private javax.swing.JButton jBtnBorrar;
    private javax.swing.JButton jBtnEditar;
    private javax.swing.JButton jBtnVerImagen;
    private javax.swing.JButton jButtonVolver;
    private javax.swing.JLabel jLabelImagen;
    private javax.swing.JList jListPersonas;
    private javax.swing.JPanel jPanelPersonas;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
