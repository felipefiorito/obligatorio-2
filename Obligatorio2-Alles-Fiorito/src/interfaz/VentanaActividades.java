package interfaz;

import dominio.Sistema;

public class VentanaActividades extends javax.swing.JFrame {

    private Sistema modelo;

    public VentanaActividades(Sistema unSistema) {
        initComponents();
        setModelo(unSistema);
    }

    public Sistema getModelo() {
        return modelo;
    }

    public void setModelo(Sistema unModelo) {
        this.modelo = unModelo;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelActividades = new javax.swing.JPanel();
        jButtonAgendarPaseo = new javax.swing.JButton();
        jButtonAgendarAlimentacion = new javax.swing.JButton();
        jButtonNuevaActividad = new javax.swing.JButton();
        jButtonVolver = new javax.swing.JButton();
        jButtonAgendarVet = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelActividades.setBackground(new java.awt.Color(143, 0, 0));

        jButtonAgendarPaseo.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonAgendarPaseo.setText("Agendar Paseo");
        jButtonAgendarPaseo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgendarPaseoActionPerformed(evt);
            }
        });

        jButtonAgendarAlimentacion.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonAgendarAlimentacion.setText("Agendar Alimentación");
        jButtonAgendarAlimentacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgendarAlimentacionActionPerformed(evt);
            }
        });

        jButtonNuevaActividad.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonNuevaActividad.setText("Registrar Actividad  Realizada");
        jButtonNuevaActividad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNuevaActividadActionPerformed(evt);
            }
        });

        jButtonVolver.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonVolver.setText("Volver");
        jButtonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverActionPerformed(evt);
            }
        });

        jButtonAgendarVet.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonAgendarVet.setText("Agendar Veterinaria");
        jButtonAgendarVet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgendarVetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelActividadesLayout = new javax.swing.GroupLayout(jPanelActividades);
        jPanelActividades.setLayout(jPanelActividadesLayout);
        jPanelActividadesLayout.setHorizontalGroup(
            jPanelActividadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelActividadesLayout.createSequentialGroup()
                .addContainerGap(134, Short.MAX_VALUE)
                .addGroup(jPanelActividadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonNuevaActividad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonAgendarAlimentacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonAgendarPaseo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonVolver, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonAgendarVet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(124, 124, 124))
        );
        jPanelActividadesLayout.setVerticalGroup(
            jPanelActividadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelActividadesLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(jButtonAgendarPaseo)
                .addGap(18, 18, 18)
                .addComponent(jButtonAgendarAlimentacion)
                .addGap(18, 18, 18)
                .addComponent(jButtonAgendarVet)
                .addGap(18, 18, 18)
                .addComponent(jButtonNuevaActividad)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                .addComponent(jButtonVolver)
                .addGap(45, 45, 45))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelActividades, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelActividades, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAgendarPaseoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgendarPaseoActionPerformed
        VentanaPaseo vent = new VentanaPaseo(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonAgendarPaseoActionPerformed

    private void jButtonAgendarAlimentacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgendarAlimentacionActionPerformed
        VentanaAlimentacion vent = new VentanaAlimentacion(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonAgendarAlimentacionActionPerformed

    private void jButtonNuevaActividadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNuevaActividadActionPerformed
        VentanaRegistrarActividad vent = new VentanaRegistrarActividad(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonNuevaActividadActionPerformed

    private void jButtonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverActionPerformed
        VentanaPrincipal vent = new VentanaPrincipal(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonVolverActionPerformed

    private void jButtonAgendarVetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgendarVetActionPerformed
        VentanaVeterinaria vent = new VentanaVeterinaria(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonAgendarVetActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAgendarAlimentacion;
    private javax.swing.JButton jButtonAgendarPaseo;
    private javax.swing.JButton jButtonAgendarVet;
    private javax.swing.JButton jButtonNuevaActividad;
    private javax.swing.JButton jButtonVolver;
    private javax.swing.JPanel jPanelActividades;
    // End of variables declaration//GEN-END:variables
}
