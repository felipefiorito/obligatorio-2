package interfaz;

import dominio.Sistema;
import java.util.Calendar;
import java.util.Date;
import javax.swing.*;

public class VentanaAlimentacion extends javax.swing.JFrame {

    private Sistema modelo;

    public VentanaAlimentacion(Sistema unSistema) {
        initComponents();
        setModelo(unSistema);
        cargarListaPerros();
        cargarListaPersonas();
        cargarCalendar();
    }

    public Sistema getModelo() {
        return modelo;
    }

    public void setModelo(Sistema unModelo) {
        this.modelo = unModelo;
    }

    public void cargarCalendar() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 1);
    }

    public void cargarListaPersonas() {
        String[] personas = new String[modelo.getListaPersonas().size()];
        jComboBoxPersonas.removeAll();
        for (int i = 0; i < modelo.getListaPersonas().size(); i++) {
            personas[i] = modelo.getListaPersonas().get(i).getNombre();
        }
        jComboBoxPersonas.setModel(new javax.swing.DefaultComboBoxModel<>(personas));
    }

    public void cargarListaPerros() {
        String[] perros = new String[modelo.getListaPerros().size()];
        jComboBoxPerros.removeAll();
        for (int i = 0; i < modelo.getListaPerros().size(); i++) {
            perros[i] = modelo.getListaPerros().get(i).getNombre();
        }
        jComboBoxPerros.setModel(new javax.swing.DefaultComboBoxModel<>(perros));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelAlimentacion = new javax.swing.JPanel();
        jLabelDia = new javax.swing.JLabel();
        jLabelResponsable = new javax.swing.JLabel();
        jComboBoxPersonas = new javax.swing.JComboBox<String>();
        jButtonAgendar = new javax.swing.JButton();
        jLabelPerro = new javax.swing.JLabel();
        jComboBoxPerros = new javax.swing.JComboBox<String>();
        jLabelHorario = new javax.swing.JLabel();
        jTextFieldHoras = new javax.swing.JTextField();
        jTextFieldMinutos = new javax.swing.JTextField();
        jLabelDospts = new javax.swing.JLabel();
        jLabelHorario1 = new javax.swing.JLabel();
        jLabelHorario2 = new javax.swing.JLabel();
        jLabelDospts1 = new javax.swing.JLabel();
        jDateChooserAlimentacion = new com.toedter.calendar.JDateChooser();
        jButtonVolver = new javax.swing.JButton();
        jLabelDia1 = new javax.swing.JLabel();
        jTextFieldAlimento = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelAlimentacion.setBackground(new java.awt.Color(143, 0, 0));

        jLabelDia.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelDia.setForeground(new java.awt.Color(255, 255, 204));
        jLabelDia.setText("Seleccione un día");

        jLabelResponsable.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelResponsable.setForeground(new java.awt.Color(255, 255, 204));
        jLabelResponsable.setText("Seleccione un responsable");

        jComboBoxPersonas.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jComboBoxPersonas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jButtonAgendar.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonAgendar.setText("Agendar");
        jButtonAgendar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgendarActionPerformed(evt);
            }
        });

        jLabelPerro.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelPerro.setForeground(new java.awt.Color(255, 255, 204));
        jLabelPerro.setText("Seleccione un/a perro/a");

        jComboBoxPerros.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jComboBoxPerros.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabelHorario.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelHorario.setForeground(new java.awt.Color(255, 255, 204));
        jLabelHorario.setText("HH");

        jTextFieldHoras.setText("12");

        jTextFieldMinutos.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jTextFieldMinutos.setText("00");

        jLabelDospts.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelDospts.setForeground(new java.awt.Color(255, 255, 204));
        jLabelDospts.setText(":");

        jLabelHorario1.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelHorario1.setForeground(new java.awt.Color(255, 255, 204));
        jLabelHorario1.setText("Ingrese horario");

        jLabelHorario2.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelHorario2.setForeground(new java.awt.Color(255, 255, 204));
        jLabelHorario2.setText("MM");

        jLabelDospts1.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelDospts1.setForeground(new java.awt.Color(255, 255, 204));
        jLabelDospts1.setText(":");

        jDateChooserAlimentacion.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jDateChooserAlimentacion.setMinSelectableDate(new java.util.Date(-62135755081000L));

        jButtonVolver.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonVolver.setText("Volver");
        jButtonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverActionPerformed(evt);
            }
        });

        jLabelDia1.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelDia1.setForeground(new java.awt.Color(255, 255, 204));
        jLabelDia1.setText("Ingrese el alimento a proporcionar");

        javax.swing.GroupLayout jPanelAlimentacionLayout = new javax.swing.GroupLayout(jPanelAlimentacion);
        jPanelAlimentacion.setLayout(jPanelAlimentacionLayout);
        jPanelAlimentacionLayout.setHorizontalGroup(
            jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAlimentacionLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelAlimentacionLayout.createSequentialGroup()
                        .addComponent(jButtonVolver)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonAgendar)
                        .addGap(24, 24, 24))
                    .addGroup(jPanelAlimentacionLayout.createSequentialGroup()
                        .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBoxPersonas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelResponsable))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelAlimentacionLayout.createSequentialGroup()
                        .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanelAlimentacionLayout.createSequentialGroup()
                                .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelPerro)
                                    .addComponent(jComboBoxPerros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 130, Short.MAX_VALUE)
                                .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelHorario1, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelAlimentacionLayout.createSequentialGroup()
                                        .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabelHorario)
                                            .addComponent(jTextFieldHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(jPanelAlimentacionLayout.createSequentialGroup()
                                                .addComponent(jLabelDospts)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jTextFieldMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelAlimentacionLayout.createSequentialGroup()
                                                .addComponent(jLabelDospts1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabelHorario2))))))
                            .addGroup(jPanelAlimentacionLayout.createSequentialGroup()
                                .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelDia)
                                    .addComponent(jDateChooserAlimentacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelDia1, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jTextFieldAlimento, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(27, 27, 27))))
        );
        jPanelAlimentacionLayout.setVerticalGroup(
            jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAlimentacionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanelAlimentacionLayout.createSequentialGroup()
                        .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelPerro)
                            .addComponent(jLabelHorario1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBoxPerros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelDospts))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelHorario)
                            .addComponent(jLabelHorario2)
                            .addComponent(jLabelDospts1))
                        .addGap(12, 12, 12)
                        .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelDia)
                            .addComponent(jLabelDia1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jDateChooserAlimentacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextFieldAlimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabelResponsable)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBoxPersonas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                .addGroup(jPanelAlimentacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonAgendar)
                    .addComponent(jButtonVolver))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelAlimentacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelAlimentacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAgendarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgendarActionPerformed
        try {
            if(this.getModelo().getListaPerros().isEmpty() 
                    || this.getModelo().getListaPersonas().isEmpty()){
                JOptionPane.showMessageDialog(rootPane,
                        "No se puede agendar alimentación hasta no haber "
                            + "registrado por lo menos una persona y un perro");
            }else{
                boolean ok = true;
                int horas = Integer.parseInt(jTextFieldHoras.getText());
                int minutos = Integer.parseInt(jTextFieldMinutos.getText());
                if (!this.getModelo().horaPermitida(horas)
                        || !this.getModelo().minutosPermitida(minutos)
                        || jTextFieldHoras.getText().isEmpty()
                        || jTextFieldMinutos.getText().isEmpty()) {
                    ok = false;
                    JOptionPane.showMessageDialog(this,
                            "La hora ingresada no es válida",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
                String hora = jTextFieldHoras.getText() + ":"
                        + jTextFieldMinutos.getText();

                Date fecha = jDateChooserAlimentacion.getDate();
                String alimento = jTextFieldAlimento.getText();
                int perro = jComboBoxPerros.getSelectedIndex();
                if (fecha == null) {
                    JOptionPane.showMessageDialog(rootPane,
                            "Debe ingresar una fecha");
                    ok = false;
                }

                if (modelo.existeAlimentacion(fecha, alimento, hora, perro)) {
                    JOptionPane.showMessageDialog(this,
                            "Ya hay una ingesta agendada para este día y hora. "
                            + "Seleccione otra fecha u horario.",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }else if (ok) {
                    this.getModelo().registrarActividad(jComboBoxPerros.getSelectedIndex(),
                            jComboBoxPersonas.getSelectedIndex(),
                            jDateChooserAlimentacion.getDate(),
                            hora, "Alimento", alimento);

                    JOptionPane.showMessageDialog(rootPane,
                             "Alimentación ingresada con éxito.");
                    VentanaActividades vent = new VentanaActividades(modelo);
                    vent.setVisible(true);
                    vent.setLocationRelativeTo(null);
                    dispose();
                }
            }
        } catch (NumberFormatException err) {
            JOptionPane.showMessageDialog(this, "Se ingreso algo distinto"
                    + " a un número en un campo en el que se pide solo números",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButtonAgendarActionPerformed

    private void jButtonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverActionPerformed
        VentanaActividades vent = new VentanaActividades(modelo);
        vent.setVisible(true);
        vent.setLocationRelativeTo(null);
        dispose();
    }//GEN-LAST:event_jButtonVolverActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAgendar;
    private javax.swing.JButton jButtonVolver;
    private javax.swing.JComboBox<String> jComboBoxPerros;
    private javax.swing.JComboBox<String> jComboBoxPersonas;
    private com.toedter.calendar.JDateChooser jDateChooserAlimentacion;
    private javax.swing.JLabel jLabelDia;
    private javax.swing.JLabel jLabelDia1;
    private javax.swing.JLabel jLabelDospts;
    private javax.swing.JLabel jLabelDospts1;
    private javax.swing.JLabel jLabelHorario;
    private javax.swing.JLabel jLabelHorario1;
    private javax.swing.JLabel jLabelHorario2;
    private javax.swing.JLabel jLabelPerro;
    private javax.swing.JLabel jLabelResponsable;
    private javax.swing.JPanel jPanelAlimentacion;
    private javax.swing.JTextField jTextFieldAlimento;
    private javax.swing.JTextField jTextFieldHoras;
    private javax.swing.JTextField jTextFieldMinutos;
    // End of variables declaration//GEN-END:variables
}
