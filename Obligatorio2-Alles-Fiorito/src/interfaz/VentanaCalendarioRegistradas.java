
package interfaz;

import dominio.Sistema;
import java.util.*;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class VentanaCalendarioRegistradas extends javax.swing.JFrame {

    Sistema modelo;

    public VentanaCalendarioRegistradas(Sistema unSistema) {
        initComponents();
        setModelo(unSistema);
        jListActividades.removeAll();
    }

    public Sistema getModelo() {
        return modelo;
    }

    public void setModelo(Sistema unModelo) {
        this.modelo = unModelo;
    }

    public void cargarLista(ArrayList<String> actPorFecha) {
        jListActividades.removeAll();
        DefaultListModel modeloLista = new DefaultListModel();
        for (int i = 0; i < actPorFecha.size(); i++) {
            modeloLista.addElement(actPorFecha.get(i));
        }
        jListActividades.setModel(modeloLista);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCalendario = new javax.swing.JPanel();
        jCalendar1 = new com.toedter.calendar.JCalendar();
        jButtonVer = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListActividades = new javax.swing.JList<>();
        jButtonDetalles = new javax.swing.JButton();
        jButtonVolver = new javax.swing.JButton();
        jLabelCalendario = new javax.swing.JLabel();
        jLabelCalendario1 = new javax.swing.JLabel();
        jLabelCalendario2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelCalendario.setBackground(new java.awt.Color(143, 0, 0));

        jButtonVer.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonVer.setText("Ver fecha");
        jButtonVer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVerActionPerformed(evt);
            }
        });

        jListActividades.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jScrollPane1.setViewportView(jListActividades);

        jButtonDetalles.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonDetalles.setText("Detalles");
        jButtonDetalles.setEnabled(false);
        jButtonDetalles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDetallesActionPerformed(evt);
            }
        });

        jButtonVolver.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jButtonVolver.setText("Volver");
        jButtonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverActionPerformed(evt);
            }
        });

        jLabelCalendario.setBackground(new java.awt.Color(255, 255, 204));
        jLabelCalendario.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelCalendario.setForeground(new java.awt.Color(255, 255, 204));
        jLabelCalendario.setText("Seleccione una fecha para ver");

        jLabelCalendario1.setBackground(new java.awt.Color(255, 255, 204));
        jLabelCalendario1.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N
        jLabelCalendario1.setForeground(new java.awt.Color(255, 255, 204));
        jLabelCalendario1.setText("las actividades del grupo");

        jLabelCalendario2.setBackground(new java.awt.Color(255, 255, 204));
        jLabelCalendario2.setFont(new java.awt.Font("Trebuchet MS", 0, 11)); // NOI18N

        javax.swing.GroupLayout jPanelCalendarioLayout = new javax.swing.GroupLayout(jPanelCalendario);
        jPanelCalendario.setLayout(jPanelCalendarioLayout);
        jPanelCalendarioLayout.setHorizontalGroup(
            jPanelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCalendarioLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCalendarioLayout.createSequentialGroup()
                        .addComponent(jButtonVolver, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCalendarioLayout.createSequentialGroup()
                        .addGroup(jPanelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCalendar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonVer, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelCalendario1)
                            .addComponent(jLabelCalendario))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                        .addGroup(jPanelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelCalendario2, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jButtonDetalles, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                        .addGap(28, 28, 28))))
        );
        jPanelCalendarioLayout.setVerticalGroup(
            jPanelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCalendarioLayout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addGroup(jPanelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCalendarioLayout.createSequentialGroup()
                        .addGroup(jPanelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCalendario)
                            .addComponent(jLabelCalendario2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCalendarioLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabelCalendario1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addComponent(jCalendar1, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonVer)
                    .addComponent(jButtonDetalles))
                .addGap(18, 18, 18)
                .addComponent(jButtonVolver)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelCalendario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelCalendario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonVerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVerActionPerformed
        Date fecha = jCalendar1.getDate();
        ArrayList<String> actPorFecha = 
                modelo.actividadesRealizadasPorFecha(fecha, 
                        modelo.getListaActividadesRealizadas());
        cargarLista(actPorFecha);
        jButtonDetalles.setEnabled(!actPorFecha.isEmpty());
    }//GEN-LAST:event_jButtonVerActionPerformed

    private void jButtonDetallesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDetallesActionPerformed
        int index = jListActividades.getSelectedIndex();
        Date fecha = jCalendar1.getDate();
        String detalles;
        if (index == -1) {
            JOptionPane.showMessageDialog(rootPane,
                    "Debe seleccionar una actividad de la lista");
        } else {
            detalles
                    = modelo.detallesActRegistradaSeleccionada(index, fecha, 
                            modelo.getListaActividadesRealizadas());
            JOptionPane.showMessageDialog(rootPane, detalles);
        }
    }//GEN-LAST:event_jButtonDetallesActionPerformed

    private void jButtonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverActionPerformed
        VentanaPrincipal vent = new VentanaPrincipal(modelo);
        vent.setLocationRelativeTo(null);
        vent.setVisible(true);
        dispose();
    }//GEN-LAST:event_jButtonVolverActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonDetalles;
    private javax.swing.JButton jButtonVer;
    private javax.swing.JButton jButtonVolver;
    private com.toedter.calendar.JCalendar jCalendar1;
    private javax.swing.JLabel jLabelCalendario;
    private javax.swing.JLabel jLabelCalendario1;
    private javax.swing.JLabel jLabelCalendario2;
    private javax.swing.JList<String> jListActividades;
    private javax.swing.JPanel jPanelCalendario;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
