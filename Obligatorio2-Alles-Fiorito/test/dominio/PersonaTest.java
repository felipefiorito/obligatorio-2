/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Administrator
 */
public class PersonaTest {
    
    public PersonaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNombre method, of class Persona.
     */
    @Test
    public void testGetNombre() {
        System.out.println("getNombre");
        Persona instance = new Persona("Jose","jose@gmail.com");
        String expResult = "Jose";
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Persona.
     */
    @Test
    public void testSetNombre() {
        System.out.println("setNombre");
        String nombre = "juan";
        Persona instance = new Persona("","");
        instance.setNombre(nombre);
        assertEquals(nombre, instance.getNombre());
    }

    /**
     * Test of getMail method, of class Persona.
     */
    @Test
    public void testGetMail() {
        System.out.println("getMail");
        Persona instance = new Persona("juan","juan@gmail.com");
        String expResult = "juan@gmail.com";
        String result = instance.getMail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMail method, of class Persona.
     */
    @Test
    public void testSetMail() {
        System.out.println("setMail");
        String mail = "prueba@gmail.com";
        Persona instance = new Persona("");
        instance.setMail(mail);
        assertEquals(instance.getMail(), mail);
    }

    /**
     * Test of getDireccionImagen method, of class Persona.
     */
    @Test
    public void testGetDireccionImagen() {
        System.out.println("getDireccionImagen");
        Persona instance = new Persona("","","imagenes/persona.png");
        String expResult = "imagenes/persona.png";
        String result = instance.getDireccionImagen();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDireccionImagen method, of class Persona.
     */
    @Test
    public void testSetDireccionImagen() {
        System.out.println("setDireccionImagen");
        String path = "imagenes/persona.png";
        Persona instance = new Persona("","","");
        instance.setDireccionImagen(path);
        assertEquals(instance.getDireccionImagen(), path);
    }

    /**
     * Test of toString method, of class Persona.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Persona instance = new Persona("jose","jose@gmail.com","imagenes/perro.gif");
        String expResult = "jose" + " - " + "jose@gmail.com";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Persona.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Persona("juan@gmail.com");
        Persona instance = new Persona("pedro@gmail.com");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
