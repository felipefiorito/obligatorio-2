/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Administrator
 */
public class PerroTest {
    
    public PerroTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNombre method, of class Perro.
     */
    @Test
    public void testGetNombre() {
        System.out.println("getNombre");
        Perro instance = new Perro("fido");
        String expResult = "fido";
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Perro.
     */
    @Test
    public void testSetNombre() {
        System.out.println("setNombre");
        String nombre = "juan";
        Perro instance = new Perro("");
        instance.setNombre(nombre);
        assertEquals("juan", instance.getNombre());
    }

    /**
     * Test of getEdad method, of class Perro.
     */
    @Test
    public void testGetEdad() {
        System.out.println("getEdad");
        Perro instance = new Perro("fido",5,"",0,0,"","");
        float expResult = 5;
        float result = instance.getEdad();
        assertEquals(expResult, result,5F);
    }

    /**
     * Test of setEdad method, of class Perro.
     */
    @Test
    public void testSetEdad() {
        System.out.println("setEdad");
        float edad = 6F;
        Perro instance = new Perro("fido",0,"",0,0,"","");
        instance.setEdad(edad);
        assertEquals(6F, instance.getEdad(),6F);
    }

    /**
     * Test of getRaza method, of class Perro.
     */
    @Test
    public void testGetRaza() {
        System.out.println("getRaza");
        Perro instance = new Perro("fido",5,"bulldog",0,0,"","");
        String expResult = "bulldog";
        String result = instance.getRaza();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRaza method, of class Perro.
     */
    @Test
    public void testSetRaza() {
        System.out.println("setRaza");
        String raza = "bulldog";
        Perro instance = new Perro("fido",5,"",0,0,"","");
        instance.setRaza(raza);
        assertEquals(raza, instance.getRaza());
    }

    /**
     * Test of getAltura method, of class Perro.
     */
    @Test
    public void testGetAltura() {
        System.out.println("getAltura");
        Perro instance = new Perro("fido",5,"",5,0,"","");
        float expResult = 5;
        float result = instance.getAltura();
        assertEquals(expResult, result,5F);
    }

    /**
     * Test of setAltura method, of class Perro.
     */
    @Test
    public void testSetAltura() {
        System.out.println("setAltura");
        float altura = 6;
        Perro instance = new Perro("fido",5,"",3,0,"","");
        instance.setAltura(altura);
        assertEquals(altura, instance.getAltura(),6F);
    }

    /**
     * Test of getPeso method, of class Perro.
     */
    @Test
    public void testGetPeso() {
        System.out.println("getPeso");
        Perro instance = new Perro("fido",5,"",6,7,"","");
        float expResult = 7;
        float result = instance.getPeso();
        assertEquals(expResult, result,7F);
    }

    /**
     * Test of setPeso method, of class Perro.
     */
    @Test
    public void testSetPeso() {
        System.out.println("setPeso");
        float peso = 1;
        Perro instance = new Perro("fido",5,"",6,7,"","");
        instance.setPeso(peso);
        assertEquals(peso, instance.getPeso(), 1F);
    }

    /**
     * Test of getComentarios method, of class Perro.
     */
    @Test
    public void testGetComentarios() {
        System.out.println("getComentarios");
        Perro instance = new Perro("fido",5,"",6,7,"asd","");
        String expResult = "asd";
        String result = instance.getComentarios();
        assertEquals(expResult, result);
    }

    /**
     * Test of setComentarios method, of class Perro.
     */
    @Test
    public void testSetComentarios() {
        System.out.println("setComentarios");
        String comentarios = "asdf";
        Perro instance = new Perro("fido",5,"",6,7,"","");
        instance.setComentarios(comentarios);
        assertEquals(comentarios, instance.getComentarios());
    }

    /**
     * Test of getDireccionImagen method, of class Perro.
     */
    @Test
    public void testGetDireccionImagen() {
        System.out.println("getDireccionImagen");
        Perro instance = new Perro("fido",5,"",6,7,"","asdf");
        String expResult = "asdf";
        String result = instance.getDireccionImagen();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDireccionImagen method, of class Perro.
     */
    @Test
    public void testSetDireccionImagen() {
        System.out.println("setDireccionImagen");
        String path = "asdf";
        Perro instance = new Perro("fido",5,"",6,7,"","");
        instance.setDireccionImagen(path);
        assertEquals(path, instance.getDireccionImagen());
    }

    /**
     * Test of toString method, of class Perro.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Perro instance = new Perro("fido",5,"",6,7,"","");
        String expResult = "fido";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Perro.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Perro("fido",5,"",6,7,"","");
        Perro instance = new Perro("juan",5,"",6,7,"","");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
