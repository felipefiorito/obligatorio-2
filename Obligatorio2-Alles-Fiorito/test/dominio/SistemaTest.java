/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Administrator
 */
public class SistemaTest {
    
    public SistemaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaPerros method, of class Sistema.
     */
    @Test
    public void testGetListaPerros() {
        System.out.println("getListaPerros");
        Sistema instance = new Sistema();
        ArrayList<Perro> expResult = new ArrayList<>();
        ArrayList<Perro> result = instance.getListaPerros();
        assertEquals(expResult, result);
    }

    /**
     * Test of agregarPerro method, of class Sistema.
     */
    @Test
    public void testAgregarPerro() {
        System.out.println("agregarPerro");
        Perro perro = new Perro("juan");
        Sistema instance = new Sistema();
        instance.agregarPerro(perro);
        assertEquals(instance.getPerro(0), perro);
    }

    /**
     * Test of agregarUnPerro method, of class Sistema.
     */
    @Test
    public void testAgregarUnPerro() {
        System.out.println("agregarUnPerro");
        String nombre = "";
        float edad = 0.0F;
        String raza = "";
        float altura = 0.0F;
        float peso = 0.0F;
        String comentario = "";
        String direccionImagen = "";
        Sistema instance = new Sistema();
        instance.agregarUnPerro(nombre, edad, raza, altura, peso, comentario, direccionImagen);
        Perro perro = new Perro(nombre,edad,raza,altura,peso,comentario,direccionImagen);
        assertEquals(instance.getPerro(0), perro);
    }

    /**
     * Test of borrarPerro method, of class Sistema.
     */
    @Test
    public void testBorrarPerro() {
        System.out.println("borrarPerro");
        Perro perro = new Perro("juan");
        Sistema instance = new Sistema();
        instance.borrarPerro(perro);
        assertEquals(!instance.hayPerro(), true);
    }

    /**
     * Test of hayPerro method, of class Sistema.
     */
    @Test
    public void testHayPerroTrue() {
        System.out.println("hayPerroTrue");
        Sistema instance = new Sistema();
        Perro perro = new Perro("juan");
        instance.agregarPerro(perro);
        boolean expResult = true;
        boolean result = instance.hayPerro();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayPerro method, of class Sistema.
     */
    @Test
    public void testHayPerroFalse() {
        System.out.println("hayPerroFalse");
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.hayPerro();
        assertEquals(expResult, result);
    }

    /**
     * Test of existeNombrePerro method, of class Sistema.
     */
    @Test
    public void testExisteNombrePerroTrue() {
        System.out.println("existeNombrePerroTrue");
        String nombre = "jose";
        Sistema instance = new Sistema();
        Perro perro = new Perro("jose");
        instance.agregarPerro(perro);
        boolean expResult = true;
        boolean result = instance.existeNombrePerro(nombre);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of existeNombrePerro method, of class Sistema.
     */
    @Test
    public void testExisteNombrePerroFalse() {
        System.out.println("existeNombrePerroFalse");
        String nombre = "jose";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.existeNombrePerro(nombre);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPerro method, of class Sistema.
     */
    @Test
    public void testGetPerro() {
        System.out.println("getPerro");
        int pos = 0;
        Sistema instance = new Sistema();
        Perro perro = new Perro("juan");
        instance.agregarPerro(perro);
        Perro result = instance.getPerro(pos);
        assertEquals(perro, result);
    }

    /**
     * Test of getListaPersonas method, of class Sistema.
     */
    @Test
    public void testGetListaPersonas() {
        System.out.println("getListaPersonas");
        Sistema instance = new Sistema();
        ArrayList<Persona> expResult = new ArrayList<>();
        ArrayList<Persona> result = instance.getListaPersonas();
        assertEquals(expResult, result);
    }

    /**
     * Test of agregarPersona method, of class Sistema.
     */
    @Test
    public void testAgregarPersona() {
        System.out.println("agregarPersona");
        Persona persona = new Persona("jose@gmail.com");
        Sistema instance = new Sistema();
        instance.agregarPersona(persona);
        assertEquals(persona, instance.getPersona(0));
    }

    /**
     * Test of agregarUnaPersona method, of class Sistema.
     */
    @Test
    public void testAgregarUnaPersona() {
        System.out.println("agregarUnaPersona");
        String nombre = "jose";
        String mail = "jose@gmail.com";
        String direccionImagen = "path/imagen.png";
        Sistema instance = new Sistema();
        instance.agregarUnaPersona(nombre, mail, direccionImagen);
        Persona persona = new Persona(nombre,mail,direccionImagen);
        assertEquals(persona, instance.getPersona(0));
    }

    /**
     * Test of borrarPersona method, of class Sistema.
     */
    @Test
    public void testBorrarPersona() {
        System.out.println("borrarPersona");
        Persona persona = new Persona("juan");
        Sistema instance = new Sistema();
        instance.agregarPersona(persona);
        instance.borrarPersona(persona);
        assertEquals(false, instance.hayPersona());
    }

    /**
     * Test of hayPersona method, of class Sistema.
     */
    @Test
    public void testHayPersonaTrue() {
        System.out.println("hayPersonaTrue");
        Sistema instance = new Sistema();
        Persona persona = new Persona("a@a.com");
        instance.agregarPersona(persona);
        boolean expResult = true;
        boolean result = instance.hayPersona();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayPersona method, of class Sistema.
     */
    @Test
    public void testHayPersonaFalse() {
        System.out.println("hayPersonaFalse");
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.hayPersona();
        assertEquals(expResult, result);
    }

    /**
     * Test of existeMailPersona method, of class Sistema.
     */
    @Test
    public void testExisteMailPersonaTrue() {
        System.out.println("existeMailPersonaTrue");
        String mail = "asd@gmail.com";
        Sistema instance = new Sistema();
        instance.agregarUnaPersona("jose", mail, "");
        boolean expResult = true;
        boolean result = instance.existeMailPersona(mail);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of existeMailPersona method, of class Sistema.
     */
    @Test
    public void testExisteMailPersonaFalse() {
        System.out.println("existeMailPersonaFalse");
        String mail = "asd@gmail.com";
        Sistema instance = new Sistema();
        instance.agregarUnaPersona("jose", "ase@hotmail.com", "");
        boolean expResult = false;
        boolean result = instance.existeMailPersona(mail);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPersona method, of class Sistema.
     */
    @Test
    public void testGetPersona() {
        System.out.println("getPersona");
        int pos = 0;
        Sistema instance = new Sistema();
        Persona persona = new Persona("asd@asd.com");
        Persona expResult = persona;
        instance.agregarPersona(persona);
        Persona result = instance.getPersona(pos);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaActividades method, of class Sistema.
     */
    @Test
    public void testGetListaActividades() {
        System.out.println("getListaActividades");
        Sistema instance = new Sistema();
        ArrayList<Actividad> expResult = new ArrayList<>();
        ArrayList<Actividad> result = instance.getListaActividades();
        assertEquals(expResult, result);
    }

    /**
     * Test of agregarActividad method, of class Sistema.
     */
    @Test
    public void testAgregarActividad() {
        System.out.println("agregarActividad");
        Sistema instance = new Sistema();
        Date fecha = new Date();
        Perro p = new Perro("wof");
        Persona persona = new Persona("prueba@gmail.com");
        Actividad a = new Actividad(p, persona,fecha,"","hola");
        instance.agregarActividad(a);
        assertEquals(a, instance.getActividad(0));
    }

    /**
     * Test of getActividad method, of class Sistema.
     */
    @Test
    public void testGetActividad() {
        System.out.println("getActividad");
        int pos = 0;
        Sistema instance = new Sistema();
        Date fecha = new Date();
        Perro p = new Perro("wof");
        Persona persona = new Persona("prueba@gmail.com");
        Actividad a = new Actividad(p, persona,fecha,"","hola");
        Actividad expResult = a;
        instance.agregarActividad(a);
        Actividad result = instance.getActividad(pos);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaActividadesRealizadas method, of class Sistema.
     */
    @Test
    public void testGetListaActividadesRealizadas() {
        System.out.println("getListaActividadesRealizadas");
        Sistema instance = new Sistema();
        ArrayList<Actividad> expResult = new ArrayList<>();
        ArrayList<Actividad> result = instance.getListaActividadesRealizadas();
        assertEquals(expResult, result);
    }

    /**
     * Test of agregarActividadRealizada method, of class Sistema.
     */
    @Test
    public void testAgregarActividadRealizada() {
        System.out.println("agregarActividadRealizada");
        Sistema instance = new Sistema();
        Date fecha = new Date();
        Perro p = new Perro("wof");
        Persona persona = new Persona("prueba@gmail.com");
        Actividad a = new Actividad(p, persona,fecha,"","hola");
        instance.agregarActividad(a);
        instance.agregarActividadRealizada(a);
        assertEquals(a, instance.getActividad(0));
    }

    /**
     * Test of getActividadRealizada method, of class Sistema.
     */
    @Test
    public void testGetActividadRealizada() {
        System.out.println("getActividadRealizada");
        int pos = 0;
        Sistema instance = new Sistema();
        Date fecha = new Date();
        Perro p = new Perro("wof");
        Persona persona = new Persona("prueba@gmail.com");
        Actividad a = new Actividad(p, persona,fecha,"","hola");
        Actividad expResult = a;
        instance.agregarActividadRealizada(a);
        Actividad result = instance.getActividadRealizada(pos);
        assertEquals(expResult, result);
    }

    /**
     * Test of cantidadNotificaciones method, of class Sistema.
     */
    @Test
    public void testCantidadNotificaciones() {
        System.out.println("cantidadNotificaciones");
        ArrayList<Actividad> actividades = new ArrayList<>();
        Perro p = new Perro("wof");
        Persona pers = new Persona("dog@gmal.com");
        Persona persona = new Persona("prueba@gmail.com");
        Date date = new Date();
        Actividad a = new Actividad(p, pers,date,"","");
        Actividad a2 = new Actividad(p, persona,date,"","");
        Actividad a3 = new Actividad(p, persona,date,"hola","hola");
        actividades.add(a);
        actividades.add(a3);
        actividades.add(a2);
        Sistema instance = new Sistema();
        int expResult = 2;
        int result = instance.cantidadNotificaciones(actividades, persona);
        assertEquals(expResult, result);
    }

    /**
     * Test of fechasNotificaciones method, of class Sistema.
     */
    @Test
    public void testFechasNotificaciones() {
        System.out.println("fechasNotificaciones");
        ArrayList<Actividad> actividades = new ArrayList<>();
        Perro p = new Perro("wof");
        Perro p2 = new Perro("dog");
        Persona pers = new Persona("dog@gmal.com");
        Persona persona = new Persona("prueba@gmail.com");
        Date date = new Date();
        Actividad a = new Actividad(p, pers,date,"","hola");
        Actividad a2 = new Actividad(p, persona,date,"","hola");
        actividades.add(a);
        actividades.add(a2);
        Sistema instance = new Sistema();
        String expResult = date.toString()+"\n";
        String result = instance.fechasNotificaciones(actividades, persona);
        assertEquals(expResult, result);
    }

    /**
     * Test of actividadesPorFecha method, of class Sistema.
     */
    @Test
    public void testActividadesPorFecha() {
        System.out.println("actividadesPorFecha");
        ArrayList<Actividad> actividades = new ArrayList<>();
        Sistema instance = new Sistema();
        Perro p = new Perro("wof");
        Perro p2 = new Perro("dog");
        Persona pers = new Persona("dog@gmal.com");
        Persona persona = new Persona("prueba@gmail.com");
        Date date = new Date();
        Actividad a = new Actividad(p, pers,date,"","hola");
        Actividad a2 = new Actividad(p, persona,date,"","hola");
        actividades.add(a);
        actividades.add(a2);
        instance.agregarActividad(a2);
        instance.agregarActividad(a);
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add(a.getPerro() + " - " + a.getTipoActividad());
        expResult.add(a2.getPerro() + " - " + a2.getTipoActividad());
        ArrayList<String> result = instance.actividadesPorFecha(date, actividades);
        assertEquals(expResult, result);
    }

    /**
     * Test of actividadesRealizadasPorFecha method, of class Sistema.
     */
    @Test
    public void testActividadesRealizadasPorFecha() {
        System.out.println("actividadesRealizadasPorFecha");
        ArrayList<Actividad> actividades = new ArrayList<>();
        Sistema instance = new Sistema();
        Perro p = new Perro("wof");
        Perro p2 = new Perro("dog");
        Persona pers = new Persona("dog@gmal.com");
        Persona persona = new Persona("prueba@gmail.com");
        Date date = new Date();
        Actividad a = new Actividad(p, pers,date,"","hola");
        Actividad a2 = new Actividad(p, persona,date,"","hola");
        actividades.add(a);
        actividades.add(a2);
        instance.agregarActividadRealizada(a2);
        instance.agregarActividadRealizada(a);
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add(a.getPerro() + " - " + a.getTipoActividad());
        expResult.add(a2.getPerro() + " - " + a2.getTipoActividad());
        ArrayList<String> result = instance.actividadesRealizadasPorFecha(date, actividades);
        assertEquals(expResult, result);
    }

    /**
     * Test of detallesActRegistradaSeleccionada method, of class Sistema.
     */
    @Test
    public void testDetallesActRegistradaSeleccionada() {
        System.out.println("detallesActRegistradaSeleccionada");
        int index = 0;
        Date fecha = new Date();
        ArrayList<Actividad> actividades = new ArrayList<>();
        Sistema instance = new Sistema();
        Perro p = new Perro("wof");
        Persona persona = new Persona("prueba@gmail.com");
        Actividad a2 = new Actividad(p, persona,fecha,"","hola");
        actividades.add(a2);
        instance.agregarActividadRealizada(a2);
        String fechaString = fecha.toString().substring(0, 10);
        String expResult = "> Perro: " + a2.getPerro() + '\n'
                + "> Tipo de actividad: " + a2.getTipoActividad() + '\n'
                + "> Persona asociada: " + a2.getPersona() + '\n'
                + "> Dia: " + fechaString + '\n';
        String result = instance.detallesActRegistradaSeleccionada(index, fecha, actividades);
        assertEquals(expResult, result);
    }

    /**
     * Test of detallesActSeleccionada method, of class Sistema.
     */
    @Test
    public void testDetallesActSeleccionada() {
        System.out.println("detallesActSeleccionada");
        int index = 0;
        Date fecha = new Date();
        ArrayList<Actividad> actividades = new ArrayList<>();
        Sistema instance = new Sistema();
        Perro p = new Perro("wof");
        Persona persona = new Persona("prueba@gmail.com");
        Actividad a2 = new Actividad(p, persona,fecha,"","hola");
        actividades.add(a2);
        instance.agregarActividad(a2);
        String fechaString = fecha.toString().substring(0, 10);
        String expResult = "> Perro: " + a2.getPerro() + '\n'
                + "> Tipo de actividad: " + a2.getTipoActividad() + '\n'
                + "> Persona asociada: " + a2.getPersona() + '\n'
                + "> Dia: " + fechaString + '\n';
        String result = instance.detallesActSeleccionada(index, fecha, actividades);
        assertEquals(expResult, result);
    }

    /**
     * Test of auxiliarBorrarPerro method, of class Sistema.
     */
    @Test
    public void testAuxiliarBorrarPerro() {
        System.out.println("auxiliarBorrarPerro");
        Perro perro = new Perro("jo@m");
        Date fecha = new Date();
        Sistema instance = new Sistema();
        Persona persona = new Persona("prueba@gmail.com");
        Actividad a2 = new Actividad(perro, persona,fecha,"","hola");
        instance.agregarActividad(a2);
        instance.auxiliarBorrarPerro(perro);
        assertEquals(instance.getListaActividades().isEmpty(), true);
    }

    /**
     * Test of auxiliarBorrarPersona method, of class Sistema.
     */
    @Test
    public void testAuxiliarBorrarPersona() {
        System.out.println("auxiliarBorrarPersona");
        Perro perro = new Perro("jo@m");
        Date fecha = new Date();
        Sistema instance = new Sistema();
        Persona persona = new Persona("prueba@gmail.com");
        Actividad a2 = new Actividad(perro, persona,fecha,"","hola");
        instance.agregarActividad(a2);
        instance.auxiliarBorrarPersona(persona);
    }

    /**
     * Test of existeAlimentacion method, of class Sistema.
     */
    @Test
    public void testExisteAlimentacion() {
        System.out.println("existeAlimentacion");
        Perro perro = new Perro("jo@m");
        Date fecha = new Date();
        Sistema instance = new Sistema();
        Persona persona = new Persona("prueba@gmail.com");
        Actividad a2 = new Actividad(perro, persona,fecha,"","hola");
        instance.agregarPerro(perro);
        String hora = "";
        String alimento = "";
        instance.agregarActividad(a2);
        int indexPerro = 0;
        boolean expResult = true;
        boolean result = instance.existeAlimentacion(fecha, alimento, hora, indexPerro);
        assertEquals(expResult, result);
    }

    /**
     * Test of registrarActividad method, of class Sistema.
     */
    @Test
    public void testRegistrarActividad() {
        System.out.println("registrarActividad");
        Perro perro1 = new Perro("jo@m");
        Date fecha = new Date();
        Sistema instance = new Sistema();
        Persona persona1 = new Persona("prueba@gmail.com");
        instance.agregarPerro(perro1);
        instance.agregarPersona(persona1);
        int perro = 0;
        int persona = 0;
        String hora = "";
        String tipo = "hola";
        Actividad a2 = new Actividad(perro1, persona1,fecha,"","hola");
        String alimento = "";
        instance.registrarActividad(perro, persona, fecha, hora, tipo, alimento);
        assertEquals(a2, instance.getActividad(0));
    }

    /**
     * Test of registrarActividadRealizada method, of class Sistema.
     */
    @Test
    public void testRegistrarActividadRealizada() {
        System.out.println("registrarActividadRealizada");
        Perro perro1 = new Perro("jo@m");
        Date fecha = new Date();
        Sistema instance = new Sistema();
        Persona persona1 = new Persona("prueba@gmail.com");
        instance.agregarPerro(perro1);
        instance.agregarPersona(persona1);
        int perro = 0;
        int persona = 0;
        String hora = "";
        String tipo = "hola";
        Actividad a2 = new Actividad(perro1, persona1,fecha,"","hola");
        instance.registrarActividadRealizada(perro, persona, fecha, hora, tipo);
        assertEquals(a2, instance.getActividadRealizada(0));
    }

    /**
     * Test of registrarPaseoRealizado method, of class Sistema.
     */
    @Test
    public void testRegistrarPaseoRealizado() {
        System.out.println("registrarPaseoRealizado");
        Perro perro1 = new Perro("jo@m");
        Date fecha = new Date();
        Sistema instance = new Sistema();
        Persona persona1 = new Persona("prueba@gmail.com");
        instance.agregarPerro(perro1);
        instance.agregarPersona(persona1);
        int perro = 0;
        int persona = 0;
        String hora = "";
        String tipo = "hola";
        Actividad a2 = new Actividad(perro1, persona1,fecha,"","hola");
        String mapaPaseo = "";
        instance.registrarPaseoRealizado(perro, persona, fecha, hora, tipo, mapaPaseo);
        assertEquals(a2, instance.getActividadRealizada(0));
    }

    /**
     * Test of existePaseo method, of class Sistema.
     */
    @Test
    public void testExistePaseo() {
        System.out.println("existePaseo");
        ArrayList<Actividad> actividades = new ArrayList<>();
        Date fecha = new Date();
        String hora = "";
        Sistema instance = new Sistema();
        Perro perro1 = new Perro("jo@m");
        Persona persona1 = new Persona("prueba@gmail.com");
        Actividad a2 = new Actividad(perro1, persona1,fecha,hora,"paseo");
        instance.agregarActividad(a2);
        actividades.add(a2);
        boolean expResult = true;
        boolean result = instance.existePaseo(actividades, fecha, hora);
        System.out.println(expResult);
        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of hayFecha method, of class Sistema.
     */
    @Test
    public void testHayFecha() {
        System.out.println("hayFecha");
        Date fecha1 = new Date();
        Sistema instance = new Sistema();
        Perro perro1 = new Perro("jo@m");
        Persona persona1 = new Persona("prueba@gmail.com");
        Actividad a2 = new Actividad(perro1, persona1,fecha1,"","paseo");
        instance.agregarActividad(a2);
        boolean expResult = true;
        boolean result = instance.hayFecha(fecha1);
        assertEquals(expResult, result);
    }

    /**
     * Test of hayFechaRealizadas method, of class Sistema.
     */
    @Test
    public void testHayFechaRealizadas() {
        System.out.println("hayFechaRealizadas");
        Date fecha1 = new Date();
        Sistema instance = new Sistema();
        Perro perro1 = new Perro("jo@m");
        Persona persona1 = new Persona("prueba@gmail.com");
        Actividad a2 = new Actividad(perro1, persona1,fecha1,"","paseo");
        instance.agregarActividadRealizada(a2);
        boolean expResult = true;
        boolean result = instance.hayFechaRealizadas(fecha1);
        assertEquals(expResult, result);
    }

    /**
     * Test of comparoFecha method, of class Sistema.
     */
    @Test
    public void testComparoFecha() {
        System.out.println("comparoFecha");
        Date fecha1 = new Date();
        Date fecha2 = new Date();
        Sistema instance = new Sistema();
        boolean expResult = true;
        boolean result = instance.comparoFecha(fecha1, fecha2);
        assertEquals(expResult, result);
    }

    /**
     * Test of edadPermitida method, of class Sistema.
     */
    @Test
    public void testEdadPermitidaMayor() {
        System.out.println("edadPermitidaMayor");
        float edad = 80F;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.edadPermitida(edad);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of edadPermitida method, of class Sistema.
     */
    @Test
    public void testEdadPermitidaMenor() {
        System.out.println("edadPermitidaMenor");
        float edad = -50F;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.edadPermitida(edad);
        assertEquals(expResult, result);
    }
    
    
    /**
     * Test of edadPermitida method, of class Sistema.
     */
    @Test
    public void testEdadPermitida() {
        System.out.println("edadPermitida");
        float edad = 20F;
        Sistema instance = new Sistema();
        boolean expResult = true;
        boolean result = instance.edadPermitida(edad);
        assertEquals(expResult, result);
    }

    /**
     * Test of alturaPermitida method, of class Sistema.
     */
    @Test
    public void testAlturaPermitidaMayor() {
        System.out.println("alturaPermitidaMayor");
        float altura = 3000F;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.alturaPermitida(altura);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of alturaPermitida method, of class Sistema.
     */
    @Test
    public void testAlturaPermitidaMenor() {
        System.out.println("alturaPermitidaMenor");
        float altura = -9F;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.alturaPermitida(altura);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of alturaPermitida method, of class Sistema.
     */
    @Test
    public void testAlturaPermitida() {
        System.out.println("alturaPermitida");
        float altura = 60F;
        Sistema instance = new Sistema();
        boolean expResult = true;
        boolean result = instance.alturaPermitida(altura);
        assertEquals(expResult, result);
    }

    /**
     * Test of pesoPermitido method, of class Sistema.
     */
    @Test
    public void testPesoPermitidoMayor() {
        System.out.println("pesoPermitidoMayor");
        float peso = 600F;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.pesoPermitido(peso);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of pesoPermitido method, of class Sistema.
     */
    @Test
    public void testPesoPermitidoMenor() {
        System.out.println("pesoPermitidoMenor");
        float peso = 0.0F;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.pesoPermitido(peso);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of pesoPermitido method, of class Sistema.
     */
    @Test
    public void testPesoPermitido() {
        System.out.println("pesoPermitido");
        float peso = 50F;
        Sistema instance = new Sistema();
        boolean expResult = true;
        boolean result = instance.pesoPermitido(peso);
        assertEquals(expResult, result);
    }

    /**
     * Test of mailPermitido method, of class Sistema.
     */
    @Test
    public void testMailPermitidoFalse() {
        System.out.println("mailPermitidoFalse");
        String mail = "asfas";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.mailPermitido(mail);
    }
    
    /**
     * Test of mailPermitido method, of class Sistema.
     */
    @Test
    public void testMailPermitidoTrue() {
        System.out.println("mailPermitidoTrue");
        String mail = "prueba@gmail.com";
        Sistema instance = new Sistema();
        boolean expResult = true;
        boolean result = instance.mailPermitido(mail);
        assertEquals(expResult, result);
    }

    /**
     * Test of horaPermitida method, of class Sistema.
     */
    @Test
    public void testHoraPermitidaMayor() {
        System.out.println("horaPermitidaMayor");
        int hora = 666;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.horaPermitida(hora);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of horaPermitida method, of class Sistema.
     */
    @Test
    public void testHoraPermitidaMenor() {
        System.out.println("horaPermitidaMenor");
        int hora = -65;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.horaPermitida(hora);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of horaPermitida method, of class Sistema.
     */
    @Test
    public void testHoraPermitida() {
        System.out.println("horaPermitida");
        int hora = 12;
        Sistema instance = new Sistema();
        boolean expResult = true;
        boolean result = instance.horaPermitida(hora);
        assertEquals(expResult, result);
    }

    /**
     * Test of minutosPermitida method, of class Sistema.
     */
    @Test
    public void testMinutosPermitidaMayor() {
        System.out.println("minutosPermitidaMayor");
        int minutos = 5151;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.minutosPermitida(minutos);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of minutosPermitida method, of class Sistema.
     */
    @Test
    public void testMinutosPermitidaMenor() {
        System.out.println("minutosPermitidaMenor");
        int minutos = -60;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.minutosPermitida(minutos);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of minutosPermitida method, of class Sistema.
     */
    @Test
    public void testMinutosPermitida() {
        System.out.println("minutosPermitida");
        int minutos = 30;
        Sistema instance = new Sistema();
        boolean expResult = true;
        boolean result = instance.minutosPermitida(minutos);
        assertEquals(expResult, result);
    }
    
}
