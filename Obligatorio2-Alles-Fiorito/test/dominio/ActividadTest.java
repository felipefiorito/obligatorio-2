/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Administrator
 */
public class ActividadTest {
    
    public ActividadTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setTipoActividad method, of class Actividad.
     */
    @Test
    public void testSetTipoActividad() {
        System.out.println("setTipoActividad");
        String tipoActividad = "paseo";
        Actividad instance = new Actividad();
        instance.setTipoActividad(tipoActividad);
        assertEquals(tipoActividad, instance.getTipoActividad());
    }

    /**
     * Test of getTipoActividad method, of class Actividad.
     */
    @Test
    public void testGetTipoActividad() {
        System.out.println("getTipoActividad");
        Actividad instance = new Actividad();
        instance.setTipoActividad("paseo");
        String expResult = "paseo";
        String result = instance.getTipoActividad();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPerro method, of class Actividad.
     */
    @Test
    public void testSetPerro() {
        System.out.println("setPerro");
        Perro perro = new Perro("juan");
        Actividad instance = new Actividad();
        instance.setPerro(perro);
        assertEquals(instance.getPerro(), perro);
    }

    /**
     * Test of getPerro method, of class Actividad.
     */
    @Test
    public void testGetPerro() {
        System.out.println("getPerro");
        Actividad instance = new Actividad();
        Perro perro = new Perro("juan");
        instance.setPerro(perro);
        Perro expResult = new Perro("juan");
        Perro result = instance.getPerro();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPersona method, of class Actividad.
     */
    @Test
    public void testSetPersona() {
        System.out.println("setPersona");
        Persona persona = new Persona("juan");
        Actividad instance = new Actividad();
        instance.setPersona(persona);
        assertEquals(instance.getPersona(), persona);
    }

    /**
     * Test of getPersona method, of class Actividad.
     */
    @Test
    public void testGetPersona() {
        System.out.println("getPersona");
        Persona persona = new Persona("juan");
        Actividad instance = new Actividad();
        instance.setPersona(persona);
        Persona expResult = persona;
        Persona result = instance.getPersona();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFecha method, of class Actividad.
     */
    @Test
    public void testSetFecha() {
        System.out.println("setFecha");
        Date fecha = new Date();
        Actividad instance = new Actividad();
        instance.setFecha(fecha);
        assertEquals(fecha, instance.getFecha());
    }

    /**
     * Test of getFecha method, of class Actividad.
     */
    @Test
    public void testGetFecha() {
        System.out.println("getFecha");
        Actividad instance = new Actividad();
        Date expResult = new Date();
        instance.setFecha(expResult);
        Date result = instance.getFecha();
        assertEquals(expResult, result);
    }

    /**
     * Test of setHorario method, of class Actividad.
     */
    @Test
    public void testSetHorario() {
        System.out.println("setHorario");
        String horario = " ";
        Actividad instance = new Actividad();
        instance.setHorario(horario);
        assertEquals(instance.getHorario(), horario);
    }

    /**
     * Test of getHorario method, of class Actividad.
     */
    @Test
    public void testGetHorario() {
        System.out.println("getHorario");
        Actividad instance = new Actividad();
        instance.setHorario("23");
        String expResult = "23";
        String result = instance.getHorario();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAlimento method, of class Actividad.
     */
    @Test
    public void testSetAlimento() {
        System.out.println("setAlimento");
        String alimento = "pasas";
        Actividad instance = new Actividad();
        instance.setAlimento(alimento);
        assertEquals(alimento, instance.getAlimento());
    }

    /**
     * Test of getAlimento method, of class Actividad.
     */
    @Test
    public void testGetAlimento() {
        System.out.println("getAlimento");
        Actividad instance = new Actividad();
        instance.setAlimento("pasas");
        String expResult = "pasas";
        String result = instance.getAlimento();
        assertEquals(expResult, result);
    }

    /**
     * Test of setServicio method, of class Actividad.
     */
    @Test
    public void testSetServicio() {
        System.out.println("setServicio");
        String servicio = "baño";
        Actividad instance = new Actividad();
        instance.setServicio(servicio);
        assertEquals(servicio, instance.getServicio());
    }

    /**
     * Test of getServicio method, of class Actividad.
     */
    @Test
    public void testGetServicio() {
        System.out.println("getServicio");
        Actividad instance = new Actividad();
        instance.setServicio("baño");
        String expResult = "baño";
        String result = instance.getServicio();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMapaPaseo method, of class Actividad.
     */
    @Test
    public void testSetMapaPaseo() {
        System.out.println("setMapaPaseo");
        String mapaPaseo = "18 de julio";
        Actividad instance = new Actividad();
        instance.setMapaPaseo(mapaPaseo);
        assertEquals(mapaPaseo,instance.getMapaPaseo());
    }

    /**
     * Test of getMapaPaseo method, of class Actividad.
     */
    @Test
    public void testGetMapaPaseo() {
        System.out.println("getMapaPaseo");
        Actividad instance = new Actividad();
        instance.setMapaPaseo("18 de julio");
        String expResult = "18 de julio";
        String result = instance.getMapaPaseo();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Actividad.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Perro perro = new Perro("juan");
        Persona persona = new Persona("jose");
        Date date = new Date();
        Object obj = new Actividad(perro,persona,date,"hora","baño");
        Actividad instance = new Actividad(perro,persona,date,"hora2","paseo");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
